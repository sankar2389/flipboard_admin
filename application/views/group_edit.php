<!-- page content -->

<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Edit Group</h3><br/>
      </div>
	   <div style="float:right;">
        <div class="col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <a href="<?php echo site_url(''); ?>group/lists"><button type="button" class="btn btn-success btn-sm">Back</button></a>
		  </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
	  <?php if ($this->session->flashdata('success_msg')) { ?>
		<div class="alert alert-success"> <?= $this->session->flashdata('success_msg') ?> </div>
	  <?php } ?>
	  <?php if ($this->session->flashdata('error_msg')) { ?>
		<div class="alert alert-error"> <?= $this->session->flashdata('error_msg') ?> </div>
	  <?php } ?>
      <div class="col-md-6 col-xs-12">
        <div class="x_panel">
          <div class="x_content">		  	
            <!-- start form for validation -->
			<?php foreach($results as $data) { ?>
            <form id="frmoperatoradd" method="post" data-parsley-validate>
              <label for="fullname">Group Name * :</label><br/><br/>
              <input type="text" id="group_name" class="form-control" name="group_name" required value="<?php echo $data->group_name; ?>" />
              <br/>
              <label for="fullname">Users * :</label><br/>
              <?php 
              $gu = $data->users;  
              $exp_user = explode(',', $gu); 
              ?>
              <select name="users[]" id="users" class="form-control" size="10" multiple>
				  <option value=""></option>              
				  <?php foreach($allusers as $u) { ?>
				  <option value="<?php echo $u->user_id; ?>" <?php  foreach($exp_user as $e) {  if($e==$u->user_id) { ?> selected="selected" <?php } } ?>>				  
				  <?php echo $u->name; ?>
				  </option>
				  <?php } ?>
              </select><span style="color:red;">Press CTRL + Select to choose multiple users</span>
              <br/><br/> 
              <p style="padding: 5px;">
              <p> <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-success" />&nbsp;
			  <input type="button" name="cancel" id="cancel" class="btn btn-primary" value="Cancel" onclick="window.location='<?php echo site_url('group/lists'); ?>'" /></p>
            </form>
			<?php } ?>
            <!-- end form for validations -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
