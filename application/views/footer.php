<div id="custom_notifications" class="custom-notifications dsp_none">

  <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">

  </ul>

  <div class="clearfix"></div>

  <div id="notif-group" class="tabbed_notifications"></div>

</div>

<script src="<?php echo site_url('assets'); ?>/js/bootstrap.min.js"></script>

<!-- bootstrap progress js -->

<script src="<?php echo site_url('assets'); ?>/js/progressbar/bootstrap-progressbar.min.js"></script>

<script src="<?php echo site_url('assets'); ?>/js/nicescroll/jquery.nicescroll.min.js"></script>

<!-- icheck -->

<script src="<?php echo site_url('assets'); ?>/js/icheck/icheck.min.js"></script>

<script src="<?php echo site_url('assets'); ?>/js/custom.js"></script>

</body>

</html>

