<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Shared Content</h3><br/>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
	  <?php if ($this->session->flashdata('success_msg')) { ?>
		<div class="alert alert-success"> <?= $this->session->flashdata('success_msg') ?> </div>
	  <?php } ?>
	  <?php if ($this->session->flashdata('error_msg')) { ?>
		<div class="alert alert-error"> <?= $this->session->flashdata('error_msg') ?> </div>
	  <?php } ?>
		
		<div class="col-md-8 col-sm-8 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><i class="fa fa-align-left"></i> By Group </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <!-- start accordion -->
                  <?php if($sharegroup>0) { ?>
                  <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
						<?php 
							foreach($sharegroup as $s) { 
								$exp_group = explode(',', $s->share_group_id);
								$i = 1;
								foreach($exp_group as $e) {
									$group_name = $this->all_group_model->groupname_byid($e);
						?>  
						<div class="panel">
						  <a class="panel-heading" role="tab" id="heading<?php echo $i; ?>" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $e; ?>" aria-expanded="true" aria-controls="collapse<?php echo $e; ?>">
							<h4 class="panel-title"><?php echo $group_name; ?></h4>
						  </a>
						  <div id="collapse<?php echo $e; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $e; ?>" aria-expanded="false">	  
							 <div class="panel-body">
							  
							  <?php $content = $this->all_group_model->get_content_bygroup($e, $user_id); ?>
							  
							  <table class="table table-bordered">
								<thead>
								  <tr>
									<th>#</th>
									<th>Users</th>
									<th>Content</th>
									<th>Date</th>
									<th>Time</th>
								  </tr>
								</thead>
								<tbody>
								  <?php 
									$j=1; 
									foreach($content as $c) { 
										$timestamp = strtotime($c->created_date);
										$date = date('d-M-Y', $timestamp);
										$time = date('h:i A', $timestamp);
										$user_name = $this->all_group_model->username_byid($c->share_user_id);
								  ?>	
								  <tr>
									<th scope="row"><?php echo $j; ?></th>
									<td><?php echo $user_name; ?></td>
									<td><?php echo $c->description; ?></td>
									<td><?php echo $date; ?></td>
									<td><?php echo $time; ?></td>
								  </tr>
								  <?php $j++; } ?>
								</tbody>
							  </table>
							 
							</div>
						  </div>
						</div><?php $i++; } ?>
						<?php } ?>
                  </div>
                  <?php } ?>
                  <!-- end of accordion -->


                </div>
              </div>
            </div>
		
    </div>
  </div>
</div>
<!-- /page content -->

<script>
$(function(){
	$('.share').click(function(){
		var id = $(this).attr('id');
		var arr = id.split('-');		
		
		var gp;
		$('.gp').each(function(){
			gp = $(this).val();
		});
		
		$.ajax({
			type : 'post',
			url : '<?php echo site_url('all_groups/share_content'); ?>', // in here you should put your query 
			data :  {from: arr[0], to: arr[1], content_id: arr[2], group: gp}, // here you pass your id via ajax .
			success : function(data)
			{  
				alert('This content has been posted.');
				return false;
			}
		});
	});
});
</script>
