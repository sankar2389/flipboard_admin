<!-- sidebar menu -->

<?php if($user_type=='admin') { ?>

<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <h3>General</h3>
    <ul class="nav side-menu">
      <li><a href="<?php echo site_url('home'); ?>"><i class="fa fa-home"></i> Home <span class="fa fa-chevron-right"></span></a></li>   
      <li><a><i class="fa fa-cog"></i> Admin 
			  <span class="fa fa-chevron-down"></span>
		  </a>
        <ul class="nav child_menu" style="display: none">

          <li><a href="<?php echo site_url('group/lists'); ?>">Company</a></li>
          <li><a href="<?php echo site_url('user/lists'); ?>">User</a></li>
	  <li><a href="<?php echo site_url('mcontent/listsAdmin'); ?>">Manage Content</a>	
          <li><a href="<?php echo site_url('mcontent/booklistsAdmin'); ?>">Book Creation</a>  
        </ul>
      </li>
      <li><a href="<?php echo site_url('home/logout'); ?>"><i class="fa fa-sign-out"></i> Logout <span class="fa fa-chevron-right"></span></a></li>   
    </ul>
  </div>
</div>

<?php } 
elseif($user_type=='company')
{
?>

<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <h3>General</h3>
    <ul class="nav side-menu">
      <!-- <li><a href="<?php echo site_url('user_home'); ?>"><i class="fa fa-home"></i> Home <span class="fa fa-chevron-right"></span></a></li> -->  
      <li><a><i class="fa fa-cog"></i> User 
	 <span class="fa fa-chevron-down"></span>
		  </a>
        <ul class="nav child_menu" style="display: none">
         <!-- <li><a href="<?php echo site_url('edit_profile'); ?>">Edit Profile</a></li> -->
          <li><a href="<?php echo site_url('change_pass_company'); ?>">Change Password</a></li>
		      <li><a href="<?php echo site_url('mcontent/listsCompany'); ?>">Manage Content</a> 		  
        </ul>
      </li>
      <li><a href="<?php echo site_url('home/logout'); ?>"><i class="fa fa-sign-out"></i> Logout <span class="fa fa-chevron-right"></span></a></li>   
       
    </ul>
  </div>
</div>

<?php
}
else { ?>

<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <h3>General</h3>
    <ul class="nav side-menu">
      <li><a href="<?php echo site_url('user_home'); ?>"><i class="fa fa-home"></i> Home <span class="fa fa-chevron-right"></span></a></li>   
      <li><a><i class="fa fa-cog"></i> User 
			  <span class="fa fa-chevron-down"></span>
		  </a>
        <ul class="nav child_menu" style="display: none">
          <li><a href="<?php echo site_url('edit_profile'); ?>">Edit Profile</a></li>
          <li><a href="<?php echo site_url('change_pass'); ?>">Change Password</a></li>
		      <li><a href="<?php echo site_url('mcontent/lists'); ?>">Manage Content</a>		  
        </ul>
      </li>
      <li><a href="<?php echo site_url('home/logout'); ?>"><i class="fa fa-sign-out"></i> Logout <span class="fa fa-chevron-right"></span></a></li>   
      <!--<li><a><i class="fa fa-share"></i> Shared Content
			  <span class="fa fa-chevron-down"></span>
		  </a>
        <ul class="nav child_menu" style="display: none">

          <li><a href="<?php //echo site_url('all_groups'); ?>">By Group</a></li>
		  <li><a href="javascript:">By User</a>
		  
        </ul>
      </li>-->
    </ul>
  </div>
</div>

<?php } ?>
<!-- /sidebar menu -->
