<!-- page content -->
<div class="right_col" role="main">
  <!-- top tiles -->
  <div class="row tile_count">
    <div>
      <div class="left"></div>
    </div>
  </div>
  <!-- /top tiles -->
  <div class="row">
    <?php if ($this->session->flashdata('success_msg')) { ?>
    <div class="alert alert-success"> <?= $this->session->flashdata('success_msg') ?> </div>
    <?php } ?>
     <h2 style="width:100%; font-size:25px;">Edit Profile</h2>
    <div class="col-md-12 col-sm-12 col-xs-12"> </div>
    <br />
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel tile fixed_height_320" style="height:600px;">
          <div align="center">
            <div class="clearfix"></div>
          </div>
          <div class="x_content">           
            <form id="frmprofile" method="post" action="<?php echo site_url('edit_profile/save') ?>" data-parsley-validate>
                  <?php foreach($users as $u) { ?>     
              <label for="name">Username * :</label><br/>
                  <input type="text" id="username" class="form-control" name="username" value="<?php echo $u->username; ?>" required /><br/>
                  <label for="name">Name * :</label><br/>
                  <input type="text" id="name" class="form-control" name="name" value="<?php echo $u->name; ?>" required /><br/>
            <label for="name">Email * :</label><br/>
                  <input type="email" id="email" class="form-control" name="email" value="<?php echo $u->email; ?>" required /><br/>
                  <label for="name">Department * :</label><br/>
                  <input type="text" id="department" class="form-control" name="department" value="<?php echo $u->department; ?>" required /><br/>
                  <label for="name">Designation * :</label><br/>
                  <input type="text" id="designation" class="form-control" name="designation" value="<?php echo $u->designation; ?>" required /><br/>             
            <?php } ?>  
                  <p> <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-success" />&nbsp;              
            </p>
            </form>
           </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /page content -->
</div>
</div>

<script src="<?php echo site_url(''); ?>/assets/js/bootstrap.min.js"></script>
<!-- bootstrap progress js -->
<script src="<?php echo site_url(''); ?>/assets/js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo site_url(''); ?>/assets/js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo site_url(''); ?>/assets/js/icheck/icheck.min.js"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="<?php echo site_url(''); ?>/assets/js/moment/moment.min.js"></script>
<script type="text/javascript" src="<?php echo site_url(''); ?>/assets/js/datepicker/daterangepicker.js"></script>
<script src="<?php echo site_url(''); ?>/assets/js/custom.js"></script>
<!-- flot js -->
<!--[if lte IE 8]><script type="text/javascript" src="<?php echo site_url(''); ?>/assets/js/excanvas.min.js"></script><![endif]-->
<!-- worldmap -->
<!-- /footer content -->
</body></html>
