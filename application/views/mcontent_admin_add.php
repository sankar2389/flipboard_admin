<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Add Content</h3>
        <br/>
      </div>
	  <div style="float:right;">
        <div class="col-xs-12 form-group pull-right top_search">
         <div class="input-group">
            <a href="<?php echo site_url(''); ?>mcontent/listsAdmin"><button type="button" class="btn btn-success btn-sm">Back</button></a>
		  </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <?php if ($this->session->flashdata('success_msg')) { ?>
      <div class="alert alert-success">
        <?= $this->session->flashdata('success_msg') ?>
      </div>
      <?php } ?>
      <?php if ($this->session->flashdata('error_msg')) { ?>
      <div class="alert alert-error">
        <?= $this->session->flashdata('error_msg') ?>
      </div>
      <?php } ?>
       <?php 
        $user = $this->session->all_userdata();
        $username = $user['logged_in']['username'];
       ?>
      <div class="col-md-8 col-xs-12">
        <div class="x_panel">
          <div class="x_content">
            <!-- start form for validation -->
            <form id="frmcontent" action="<?php site_url('mcontent/addadmin'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
              <label for="title">Type * :</label>
              <br/>
              <input type="radio" id="image" name="type" value="Image" checked="checked" onclick="return fnType(this.value);" />&nbsp;&nbsp;Image&nbsp;&nbsp;
              <input type="radio" id="video" name="type" value="Video" onclick="return fnType(this.value);" />&nbsp;&nbsp;Video<br/><br/>
              <label for="title">Title * :</label>
              <br/>
              <input type="text" id="title" class="form-control" name="title" placeholder="Title" required />
              <br/>
              <label for="description">Description * :</label>
              <br/>
              <textarea name="description" id="description" required style="width:100%; height:110px;" placeholder="Description" required></textarea>
              <br/><br/>      
              
              <label for="title">Post URL * : </label>
              <br/>
              <input type="text" id="post_url" class="form-control" name="post_url" placeholder="http://google.com"    required /> 
             <span style="color:blue;font-size:11px;">Ex: http://google.com</span>
               <br/><br/> 
               
              <label for="title">Author * :</label>
              <br/>
              <input type="text" id="author" class="form-control" name="author" value="<?php echo $username; ?>" readonly="" /> 
               <br/><br/>  
              
               <div class="block_image" style="border:1px solid aqua; padding:10px;">		
				    <label for="image" id="label_upimage" style="display:block;">Upload Image: * :</label> 			
					<input type="file" name="upimage[]" id="upimage" /><button class="btn btn-success btn-xs add_field_button" style="float:right;">Add More Fields</button><br/>
					<input type="text" name="upimage_title[]" value="" placeholder="Enter the Title" style="width:300px;" />
			   </div>
				

               <div class="block_video" style="display:none;">
				  <label for="video" id="label_upvideo">Youtube Video URL * :</label>
				  <input type="text" id="upvideo" name="upvideo[]" class="form-control" onchange="return validateYouTubeUrl();" />
				  <span style="color:blue;font-size:11px;">Ex: https://www.youtube.com/watch?v=IVx6ZlksMJw</span>
               </div> <br/>
               
              <strong>Book(s)</strong><br/>

<select name="book[]" id='book' required>
  <option value="">----Select Book---</option>
 <?php foreach($books as $g) { ?>
  <option value="<?php echo $g->bk_id; ?>"><?php echo $g->book_title; ?></option>
 <?php } ?>
</select>
               <!--<select name="book" id="book" class="form-control" size="10"  >
             
              <?php foreach($books as $g) { ?>
              <option value="<?php echo $g->bk_id; ?>"><?php echo $g->book_title; ?></option>
              <?php } ?>
              </select>--><!--<span style="color:blue;font-size:11px;">Press CTRL + Select to choose multiple groups</span>--><br/><br/>

             <!-- <div id='normal_user_div'>
              <strong>Users</strong><br/>
               <select name="users[]" id="users" class="form-control" size="10" required multiple>
              <option value=""></option>
              <?php foreach($users as $g) { ?>
              <option value="<?php echo $g->user_id; ?>"><?php echo $g->name; ?></option>
              <?php } ?>
              </select><span style="color:blue;font-size:11px;">Press CTRL + Select to choose multiple users</span><br/><br/>
              </div>-->
<br>
              <div id='ajax_user_div'>             
              </div>

              <p>
                <input type="submit" name="submit" id="submit" value="Publish"  class="btn btn-success" />
                &nbsp;
                <input type="button" name="cancel" id="cancel"  class="btn btn-primary" value="Cancel" onclick="window.location='<?php echo site_url('mcontent/listsAdmin'); ?>'" />
              </p>          
            </form>
            <!-- end form for validations -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->

<style>.remove_field { color:red; }</style>
			   
<script type="text/javascript">
	
	function fnType(val)
	{
		if(val=='Image'){
			$('.block_video').hide();
			$('.block_image').show();
		} else {
			$('.block_video').show();
			$('.block_image').hide();
		}
	}
	
	$(document).ready(function() {
		
		var max_fields      = 20; //maximum input boxes allowed
		var wrapper         = $(".block_image"); //Fields wrapper
		var add_button      = $(".add_field_button"); //Add button ID
		
		var x = 1; //initlal text box count
		$(add_button).click(function(e){ //on add input button click
			e.preventDefault();
			if(x < max_fields){ //max input box allowedWord Counter is a word count and a character count tool. Simply place your cursor into the box and begin typing. Word counter will automatically count the number of words and characters as you type. You can also copy and paste a document you have already written into the word counter box and it will display the word count and character numbers for that piece of writing.
				x++; //text box increment
				$(wrapper).append('<div><br/><input type="file" name="upimage[]" id="upimage" /><br/><input type="text" name="upimage_title[]" placeholder="Enter the Title" value="" style="width:300px;" />&nbsp;&nbsp;<a href="#" class="remove_field">Remove</a></div>'); //add input box
			}
		});
		
		$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
			e.preventDefault(); $(this).parent('div').remove(); x--;
		});
		
		$( "#group" ).click(function() {
			
			var foo = []; 
			$('#group :selected').each(function(i, selected){ 
			  foo[i] = $(selected).val(); 
			});

			 $.ajax({
		        type : 'post',
		        url : '<?php echo site_url('mcontent/user_notin_group'); ?>',
		        data :  'gIDs='+ foo, 
		        success : function(response)
		        { 
		            $('#normal_user_div').hide();
		            $('#ajax_user_div').html(response);
		        }
		      });

		});
		
	});


	function validateYouTubeUrl() {    
	    var url = $('#upvideo').val();
	    if (url != undefined || url != '') {        
	        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
	        var match = url.match(regExp);
	        if (match && match[2].length == 11) {
	            // Do anything for being valid
	            // if need to change the url to embed url then use below line            
	            //$('#videoObject').attr('src', 'https://www.youtube.com/embed/' + match[2] + '?autoplay=1&enablejsapi=1');
	        } else {
	            alert('Youtube video url is not valid');
	            // Do anything for not being valid
	        }
	    }
	}


	/*function checkDomain(nname)
	{
		var arr = new Array('.com','.net','.org','.biz','.coop','.info','.museum','.name','.pro','.edu','.gov','.int','.mil','.ac','.ad','.ae','.af','.ag',
'.ai','.al','.am','.an','.ao','.aq','.ar','.as','.at','.au','.aw','.az','.ba','.bb','.bd','.be','.bf','.bg','.bh','.bi','.bj','.bm',
'.bn','.bo','.br','.bs','.bt','.bv','.bw','.by','.bz','.ca','.cc','.cd','.cf','.cg','.ch','.ci','.ck','.cl','.cm','.cn','.co','.cr',
'.cu','.cv','.cx','.cy','.cz','.de','.dj','.dk','.dm','.do','.dz','.ec','.ee','.eg','.eh','.er','.es','.et','.fi','.fj','.fk','.fm',
'.fo','.fr','.ga','.gd','.ge','.gf','.gg','.gh','.gi','.gl','.gm','.gn','.gp','.gq','.gr','.gs','.gt','.gu','.gv','.gy','.hk','.hm',
'.hn','.hr','.ht','.hu','.id','.ie','.il','.im','.in','.io','.iq','.ir','.is','.it','.je','.jm','.jo','.jp','.ke','.kg','.kh','.ki',
'.km','.kn','.kp','.kr','.kw','.ky','.kz','.la','.lb','.lc','.li','.lk','.lr','.ls','.lt','.lu','.lv','.ly','.ma','.mc','.md','.mg',
'.mh','.mk','.ml','.mm','.mn','.mo','.mp','.mq','.mr','.ms','.mt','.mu','.mv','.mw','.mx','.my','.mz','.na','.nc','.ne','.nf','.ng',
'.ni','.nl','.no','.np','.nr','.nu','.nz','.om','.pa','.pe','.pf','.pg','.ph','.pk','.pl','.pm','.pn','.pr','.ps','.pt','.pw','.py',
'.qa','.re','.ro','.rw','.ru','.sa','.sb','.sc','.sd','.se','.sg','.sh','.si','.sj','.sk','.sl','.sm','.sn','.so','.sr','.st','.sv',
'.sy','.sz','.tc','.td','.tf','.tg','.th','.tj','.tk','.tm','.tn','.to','.tp','.tr','.tt','.tv','.tw','.tz','.ua','.ug','.uk','.um',
'.us','.uy','.uz','.va','.vc','.ve','.vg','.vi','.vn','.vu','.ws','.wf','.ye','.yt','.yu','.za','.zm','.zw');

		var mai = nname;
		var val = true;
		var dot = mai.lastIndexOf(".");
		var dname = mai.substring(0,dot);
		var ext = mai.substring(dot,mai.length);
		if(dot>2 && dot<57)
		{
			for(var i=0; i<arr.length; i++)
			{
					if(ext == arr[i])
					{
						val = true;
					break;
					}	
					else
					{
						val = false;
				}
			}
			if(val == false)
			{
	 			alert("Your domain extension "+ext+" is not correct");
 				return false;
			}
			else
			{
				for(var j=0; j<dname.length; j++)
				{
  					var dh = dname.charAt(j);
  					var hh = dh.charCodeAt(0);
  					if((hh > 47 && hh<59) || (hh > 64 && hh<91) || (hh > 96 && hh<123) || hh==45 || hh==46)
  					{
	 					if((j==0 || j==dname.length-1) && hh == 45)	
  	 					{
 	  	 					alert("Domain name should not begin are end with '-'");
	      					return false;
 	 					}
  					}
					else	
					{
  	 					alert("Your domain name should not have special characters");
	 					return false;
  					}
				}
			}
		}
		else
		{
				alert("invalid domain name");pattern="https?://.+"
				return false;
		}	
		return true;
	}*/
</script>
