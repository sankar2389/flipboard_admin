<!-- page content -->
<div class="right_col" role="main">
  <!-- top tiles -->
  <div class="row tile_count">
    <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
      <div class="left"></div>
      <div class="right"> <span class="count_top"><i class="fa fa-user"></i> Total Users</span>
        <div class="count blue"><?php $this->db->where('user_type', 'user'); $this->db->where('status' , 1); echo $this->db->count_all_results('user'); ?></div>
        <span class="count_bottom"><a href="<?php echo site_url('user/lists'); ?>" style="color:#337ab7">View All Users</a></span> </div>
    </div>
    <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
      <div class="left"></div>
      <div class="right"> <span class="count_top"><i class="fa fa-clock-o"></i> Total Groups</span>
        <div class="count green"><?php $this->db->where('status' , 1); echo $this->db->count_all_results('group'); ?></div>
        <span class="count_bottom"><a href="<?php echo site_url('group/lists'); ?>" style="color:#337ab7">View All Groups</a></span> </div>
    </div>

    <div>
      <div class="left"></div>
    </div>
  </div>
  <!-- /top tiles -->
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12"> </div>
    <br />
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel tile fixed_height_320" style="height:600px;">
          <div align="center">            
            <div class="clearfix"></div>
          </div>
          <div class="x_content"> </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /page content -->
</div>
</div>
<div id="custom_notifications" class="custom-notifications dsp_none">
  <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
  </ul>
  <div class="clearfix"></div>
  <div id="notif-group" class="tabbed_notifications"></div>
</div>
<script src="<?php echo site_url(''); ?>/assets/js/bootstrap.min.js"></script>
<!-- bootstrap progress js -->
<script src="<?php echo site_url(''); ?>/assets/js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo site_url(''); ?>/assets/js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo site_url(''); ?>/assets/js/icheck/icheck.min.js"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="<?php echo site_url(''); ?>/assets/js/moment/moment.min.js"></script>
<script type="text/javascript" src="<?php echo site_url(''); ?>/assets/js/datepicker/daterangepicker.js"></script>
<script src="<?php echo site_url(''); ?>/assets/js/custom.js"></script>
<!-- flot js -->
<!--[if lte IE 8]><script type="text/javascript" src="<?php echo site_url(''); ?>/assets/js/excanvas.min.js"></script><![endif]-->
<!-- worldmap -->
<!-- /footer content -->
</body></html>
