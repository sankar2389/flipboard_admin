<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Sankar Flipboard</title>
<!-- Bootstrap core CSS -->
<link href="<?php echo site_url('assets'); ?>/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo site_url('assets'); ?>/fonts/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo site_url('assets'); ?>/css/animate.min.css" rel="stylesheet">
<!-- Custom styling plus plugins -->
<link href="<?php echo site_url('assets'); ?>/css/custom.css" rel="stylesheet">
<link rel="<?php echo site_url('assets'); ?>/stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.3.css" />
<link href="<?php echo site_url('assets'); ?>/css/icheck/flat/green.css" rel="stylesheet" />
<link href="<?php echo site_url('assets'); ?>/css/floatexamples.css" rel="stylesheet" type="text/css" />
<script src="<?php echo site_url(''); ?>/assets/js/jquery.min.js"></script>
<script src="<?php echo site_url(''); ?>/assets/js/nprogress.js"></script>
<link rel="icon" type="image/png" href="<?php echo site_url('assets/images/favicon-32x32.png'); ?>" sizes="32x32" />
<link rel="icon" type="image/png" href="<?php echo site_url('assets/images/favicon-16x16.png'); ?>" sizes="16x16" />
<!--[if lt IE 9]>
        <script src="../assets/<?php echo site_url(''); ?>/assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
<body class="nav-md">
<div class="container body">
<div class="main_container">
<div class="col-md-3 left_col">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;"> <a href="<?php echo site_url('home'); ?>" class="site_title"> <img src="<?php echo site_url(''); ?>assets/images/logo1.png" style="width:32px; height:32px;" />&nbsp;Sankar Flipboard!</a> </div>
    <div class="clearfix"></div>
    <!-- menu prile quick info -->
    <div class="profile">
      <div class="profile_pic"> <img src="<?php echo site_url('assets'); ?>/images/admin_img.gif" alt="..." class="img-circle profile_img"> </div>
      <div class="profile_info"> <span>Welcome,</span>
        <h2><?php echo $username; ?></h2>
      </div>
    </div>
    <!-- /menu prile quick info -->
    <br />
    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <?php $this->load->view('left_menu'); ?>
    </div>
    <!-- /sidebar menu -->
    <!-- /menu footer buttons -->
    <!--<div class="sidebar-footer hidden-small">		
    <a href="<?php echo site_url('home'); ?>" data-toggle="tooltip" data-placement="top" title="Home"> 
    <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span> </a> 
    <a href="<?php echo site_url('home/logout'); ?>" 
    data-toggle="tooltip" data-placement="top" title="Logout"> <span class="glyphicon glyphicon-off" aria-hidden="true"></span> </a> 
    </div>-->
    <!-- /menu footer buttons -->
  </div>
</div>
<?php $this->load->view('top_nav'); ?>
