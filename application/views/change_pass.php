<!-- page content -->
<div class="right_col" role="main">
  <!-- top tiles -->
  <div class="row tile_count">
    <div>
      <div class="left"></div>
    </div>
  </div>
  <!-- /top tiles -->
  <div class="row">
    <?php if ($this->session->flashdata('success_msg')) { ?>
    <div class="alert alert-success"> <?= $this->session->flashdata('success_msg') ?> </div>
    <?php } ?>
    <?php if ($this->session->flashdata('error_msg')) { ?>
    <div class="alert alert-error"> <?= $this->session->flashdata('error_msg') ?> </div>
    <?php } ?>
     <h2 style="width:100%; font-size:25px;">Change Password</h2>
    <div class="col-md-12 col-sm-12 col-xs-12"> </div>
    <br />
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel tile fixed_height_320">
          <div align="center">
            <div class="clearfix"></div>
          </div>
          <div class="x_content">             
            <form id="frmchangepass" method="post" action="<?php echo site_url('change_pass/save') ?>" onsubmit="return fnChangePass();" data-parsley-validate>              
            <label for="name">Current password * :</label><br/>
            <input type="password" id="current_pass" class="form-control" name="current_pass" required /><br/>
            <label for="name">New password * :</label><br/>
            <input type="password" id="new_pass" class="form-control" name="new_pass" required /><br/>
            <label for="name">Confirm new password * :</label><br/>
            <input type="password" id="confirm_new_pass" class="form-control" name="confirm_new_pass" required /><br/><br/>
            <p> <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-success" />&nbsp;             
            </p>
            </form>
           </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /page content -->
</div>
</div>

<script>
function fnChangePass()
{
    var new_pass = document.getElementById('new_pass').value;
    var confirm_new_pass = document.getElementById('confirm_new_pass').value;
    if(new_pass!=confirm_new_pass)
    {
        alert('New password and confirm new password should be same');
        return false;
    }
}
</script>

<script src="<?php echo site_url(''); ?>/assets/js/bootstrap.min.js"></script>
<!-- bootstrap progress js -->
<script src="<?php echo site_url(''); ?>/assets/js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo site_url(''); ?>/assets/js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo site_url(''); ?>/assets/js/icheck/icheck.min.js"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="<?php echo site_url(''); ?>/assets/js/moment/moment.min.js"></script>
<script type="text/javascript" src="<?php echo site_url(''); ?>/assets/js/datepicker/daterangepicker.js"></script>
<script src="<?php echo site_url(''); ?>/assets/js/custom.js"></script>
<!-- flot js -->
<!--[if lte IE 8]><script type="text/javascript" src="<?php echo site_url(''); ?>/assets/js/excanvas.min.js"></script><![endif]-->
<!-- worldmap -->
<!-- /footer content -->
</body></html>
