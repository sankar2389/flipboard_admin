<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Add User</h3><br/>
      </div>
	  <div style="float:right;">
        <div class="col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <a href="<?php echo site_url(''); ?>user/lists"><button type="button" class="btn btn-success btn-sm">Back</button></a>
		  </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
	  <?php if ($this->session->flashdata('success_msg')) { ?>
		<div class="alert alert-success"> <?= $this->session->flashdata('success_msg') ?> </div>
	  <?php } ?>
	  <?php if ($this->session->flashdata('error_msg')) { ?>
		<div class="alert alert-error"> <?= $this->session->flashdata('error_msg') ?> </div>
	  <?php } ?>
      <div class="col-md-6 col-xs-12">
        <div class="x_panel">
          <div class="x_content">		  	
            <!-- start form for validation -->
            <form id="frmoperatoradd" method="post" data-parsley-validate>
              <label for="name">Name * :</label>
              <input type="text" id="name" class="form-control" name="name" required /><br/>
              <label for="email">Email Id * :</label>
              <input type="text" id="email" class="form-control" name="email" required /><br/>
              <label for="dept">Department * :</label>
              <input type="text" id="department" class="form-control" name="department" required /><br/>
              <label for="designation">Designation * :</label>
              <input type="text" id="designation" class="form-control" name="designation" required /><br/>
              <label for="group">Company * :</label>
              <select name="group[]" id="group" class="form-control" size="10" >
              <option value=""></option>
              <?php foreach($group as $g) { ?>
              <option value="<?php echo $g->group_id; ?>"><?php echo $g->group_name; ?></option>
              <?php } ?>
              </select><span style="color:red;"><!--Press CTRL + Select to choose multiple groups--></span>
              <br/><br/>              
             
              <p> <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-success" />&nbsp;
			  <input type="button" name="cancel" id="cancel" class="btn btn-primary" value="Cancel" onclick="window.location='<?php echo site_url('user/lists'); ?>'" /></p>
            </form>
            <!-- end form for validations -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
