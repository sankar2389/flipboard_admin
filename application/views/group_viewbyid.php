<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>View Company</h3>
      </div>
	   <div style="float:right;">
        <div class="col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <a href="<?php echo site_url(''); ?>group/lists"><button type="button" class="btn btn-success btn-sm">Back</button></a>
		  </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel" style="height:auto;">
          <div class="x_title">
            <table class="table table-striped projects">
              <tbody>
                <?php foreach($results as $data) { ?>
                <tr>
                  <td style="width:130px;"><strong>Name:</strong> </td><td><?php echo $data->group_name; ?></td>               
                </tr>
                		<tr>
                  <td><strong>Users:</strong> </td><td>
      					  <?php 
      					  $users = $data->users; 
      					  $exp_user = explode(',', $users);
      					  foreach($exp_user as $val) {
      						  echo $this->group_model->get_user_byid($val);
      						  echo '<br/>';
      					  }
      					  ?></td>
                </tr>
                <tr>
                  <td><strong>Status:</strong> </td><td><?php if($data->status==1) echo 'Active'; else echo 'InActive'; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
