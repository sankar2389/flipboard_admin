<!-- top navigation -->
<div class="top_nav">
  <div class="nav_menu">
    <nav class="" role="navigation">
      <div class="nav toggle"> <a id="menu_toggle"><i class="fa fa-bars"></i></a> </div>      
      <ul class="nav navbar-nav navbar-right">
        <li class=""> <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <img src="<?php echo site_url('assets'); ?>/images/admin_img.gif" alt=""><?php echo $username; ?> <span class=" fa fa-angle-down"></span> </a>
          <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
            <li><a href="javascript:;"> Profile</a> </li>
            <li> <a href="javascript:;">Help</a> </li>
            <li><a href="<?php echo site_url('home/logout'); ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a> </li>
          </ul>
        </li>            
      </ul>
    </nav>
    <h2 style="text-align:center; font-size:22px;">Welcome to Sankar Flipboard <?php echo ucfirst($user_type); ?> Panel</h2>
    <span style="float:right;">
    </span>
  </div>
</div>
<!-- /top navigation -->
