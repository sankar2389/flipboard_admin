<!-- Custom styling plus plugins -->
<link href="<?php echo base_url(); ?>/assets/css/icheck/flat/green.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>/assets/css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">
<!-- page content -->
<div class="right_col" role="main">   
  <div class="">
    <div class="clearfix"></div>
    <div class="row">
      <?php if ($this->session->flashdata('success_msg')) { ?>
      <div class="alert alert-success"> <?= $this->session->flashdata('success_msg') ?> </div>
      <?php } ?>
      <?php if ($this->session->flashdata('error_msg')) { ?>
      <div class="alert alert-error"> <?= $this->session->flashdata('error_msg') ?> </div>
      <?php } ?>
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2 style="width:100%;">Manage Content <span style="float:right;"><a href="add"><button type="button" class="btn btn-success btn-sm">Add Content</button></a></span></h2>
            <div class="clearfix"></div>
          </div>
          
          <div class="x_content">
            <form method="post" name="frm_list" id="frm_list" action="<?php echo site_url('mcontent/formsubmit'); ?>">
            <table id="example" class="table table-striped responsive-utilities jambo_table">
              <thead>
                <tr class="headings">
                  <th width="3%">Select</th>
                  <th width="15%">Title</th>
                  <th width="15%">Description</th>
                  <th width="15%">Date</th>
                  <th width="15%">Time</th>
                  <th width="5%">Status </th>
                  <th width="20%">Action </th>
                </tr>
              </thead>
              <tbody>
                <?php          
                if($this->uri->segment(3))
                  $i = $this->uri->segment(3);
                else
                  $i = 0; 
                if($results>0) 
                {
                  foreach($results as $data) 
                  { 
                    $i++; 
                    if ($i % 2 == 0)
                    $oe = "even";
                    else
                    $oe = "odd";
                    $date = date('d-M-Y',strtotime($data->created_date));
                    $time = date('h:i a',strtotime($data->created_date));
                ?>
                <tr class="<?php echo $oe; ?> pointer">
                  <td class=" "><input name="chkall[]" id="chkall" class="chkall" type="checkbox" value="<?php echo $data->mc_id; ?>"></td>
                  <td class=" "><?php echo $data->title; ?></td>
                  <td class=" "><?php echo substr($data->description,0,255); ?></td>
                  <td class=" "><?php echo $date; ?></td>
                  <td class=" "><?php echo $time; ?></td>
                  <td style="width:100px;">
                  <?php if($data->status==1) 
                  echo '<a href="'.site_url('mcontent/status/inactive/'.$data->mc_id).'"><button type="button" class="btn btn-success btn-xs">Active</button></a>'; 
                  else 
                  echo '<a href="'.site_url('mcontent/status/active/'.$data->mc_id).'"><button type="button" class="btn btn-danger btn-xs">InActive</button></a>'; 
                  ?></td>
                  <td style="width: 20%; position: relative;"><a href="<?php echo site_url('mcontent/view/'.$data->mc_id); ?>" class="btn btn-primary btn-xs">
                  <i class="fa fa-folder"></i> View </a> <a href="<?php echo site_url('mcontent/edit/'.$data->mc_id); ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a> <a href="<?php echo site_url('mcontent/delete/'.$data->mc_id); ?>" class="btn btn-danger btn-xs"  onclick="return confirm('Are you sure you want to delete this group?');"><i class="fa fa-trash-o"></i> Delete </a> </td>
                </tr>
                <?php } 
                } else { 
                ?>
                <tr>
                  <td colspan="8" style="color: #FF3300;">No content found</td>
                </tr>
                <?php } ?>              
              </tbody>
            </table>
            <div class="box-footer clearfix">
                <div class="col-sm-12 clearfix">&nbsp;
                                  </div>
                <div class="col-sm-12" style="text-align:center;padding-top:18px;font-size: 14px;font-weight: bold;">Action :
                  <select style="width:100px; margin-right:5px; padding:5px;" name="action" id="action">
                    <option value="Inactive">Inactive </option>
                    <option value="Active">Active </option>
                    <option value="Delete">Delete</option>
                  </select>
                  <button name="btn_action" class="btn btn-primary btn-sm " type="submit" value="Action" id="btn_action" onclick="return check_confirm('Are you sure want to do the action?');"><i class="fa fa-bolt"></i> Action</button>
                  <button class="btn btn-primary btn-sm" type="button" onclick="if(markAll()) return false;">Select all</button>
                  <button class="btn btn-primary btn-sm" type="button" onclick="if(unmarkAll()) return false;">Deselect All</button>
                </div>
              </div>
              </form>
          </div>
        </div>
      </div>
      <br />
      <br />
      <br />
    </div>
  </div>
</div>
<!-- /page content -->
<style>.DTTT_container { display:none !important; }</style>

<script type="text/javascript" language="javascript" charset="utf-8" src="<?php echo base_url(); ?>/assets/js/checkbox.js"></script> 

<!-- Datatables -->
<script src="<?php echo base_url(); ?>/assets/js/datatables/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/datatables/tools/js/dataTables.tableTools.js"></script>
<script>
    $(document).ready(function() {
      $('input.tableflat').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
      });
    });

    var asInitVals = new Array();
    $(document).ready(function() {
      var oTable = $('#example').dataTable({
      "oLanguage": {
        "sSearch": "Search all columns:"
      },
      "aoColumnDefs": [{
        'bSortable': false,
        //'aTargets': [0],
        
        } //disables sorting for column one
      ],
      "aLengthMenu": [[10, 20, 50, 75, -1], [10, 20, 50, 75, "All"]],
      'iDisplayLength': 20,
      //"pageLength": 2,
      "sPaginationType": "full_numbers",
      "dom": 'T<"clear">lfrtip',
      //"tableTools": {
        //"sSwfPath": "js/datatables/tools/swf/copy_csv_xls_pdf.swf"
      //}
      });      
    });    
</script>
