<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>View Content</h3>
      </div>
	   <div style="float:right;">
        <div class="col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <a href="<?php echo site_url(''); ?>mcontent/booklistsAdmin"><button type="button" class="btn btn-success btn-sm">Back</button></a>
		  </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel" style="height:auto;">
          <div class="x_title">
            <table class="table table-striped projects">
              <tbody>
                <?php foreach($results as $data) { 
        	$date = date('d-M-Y',strtotime($data->created_date));
        	$time = date('h:i a',strtotime($data->created_date));					
        				?>
                <tr>
                  <td style="width:130px;"><strong>Title:</strong> </td><td><?php echo $data->book_title; ?></td>               
                </tr>
                 	
                <tr>
                  <td style="width:130px;"><strong>Date:</strong> </td><td><?php echo $date; ?></td>               
                </tr>
                <tr>
                  <td style="width:130px;"><strong>Time:</strong> </td><td><?php echo $time; ?></td>               
                </tr>
                         
                
                <tr>
                  <td style="width:130px;"><strong>Image:</strong> </td><td>
					   
 <img src="<?php echo site_url(''); ?>uploads/bookcontent/<?php echo $data->img_path; ?>" border="0" width="160" height="100" /><br/>	<br/>  
					   
				  </td>               
                </tr>
                
                 
                
                 
               
                
                <tr>
                  <td><strong>Status:</strong> </td><td><?php if($data->active==1) echo 'Active'; else echo 'InActive'; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
