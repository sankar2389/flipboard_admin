<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>View Content</h3>
      </div>
	   <div style="float:right;">
        <div class="col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <a href="<?php echo site_url(''); ?>mcontent/listsCompany"><button type="button" class="btn btn-success btn-sm">Back</button></a>
		  </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel" style="height:auto;">
          <div class="x_title">
            <table class="table table-striped projects">
              <tbody>
                <?php foreach($results as $data) { 
        	$date = date('d-M-Y',strtotime($data->created_date));
        	$time = date('h:i a',strtotime($data->created_date));					
        				?>
                <tr>
                  <td style="width:130px;"><strong>Title:</strong> </td><td><?php echo $data->title; ?></td>               
                </tr>
                <tr>
                  <td style="width:130px;"><strong>Description:</strong> </td><td><?php echo $data->description; ?></td>               
                </tr>	
                <tr>
                  <td style="width:130px;"><strong>Date:</strong> </td><td><?php echo $date; ?></td>               
                </tr>
                <tr>
                  <td style="width:130px;"><strong>Time:</strong> </td><td><?php echo $time; ?></td>               
                </tr>
              <!--  <tr>
                  <td style="width:130px;"><strong>Group:</strong> </td><td><?php echo $this->mcontent_model->groupname_byid($data->share_group_id); ?></td>               
                </tr> -->
                <tr>
                  <td style="width:130px;"><strong>Users:</strong> </td><td><?php echo $this->mcontent_model->username_byid($data->share_user_id); ?></td>               
                </tr>              
                
                <tr>
                  <td style="width:130px;"><strong>Image:</strong> </td><td>
					  <?php 
					  $image = $this->mcontent_model->get_image_comp($data->mc_id); 
					  if($image) 
					  {
						  foreach($image as $img) 
						  {	
					  ?>
						  <img src="<?php echo site_url(''); ?>uploads/mcontent_company/<?php echo $img->image; ?>" border="0" width="160" height="100" /><br/>	<br/>  
					  <?php 
						  } 
					  } ?>
				  </td>               
                </tr>
                
                 <tr>
                  <td style="width:130px;"><strong>Video URL:</strong> </td><td>
					  <?php 
					  $vidurl = $this->mcontent_model->get_video_url_comp($data->mc_id); 
					  if($vidurl) 
					  {
						  foreach($vidurl as $v) 
						  {	
					  ?>
						  <?php echo $v->video_url; ?><br/>	<br/>  
					  <?php 
						  } 
					  } ?>
				  </td>               
                </tr>
                
                <tr>
                  <td><strong>Post URL:</strong> </td><td><?php echo $data->post_url; ?></td>
                </tr>
                
                <tr>
                  <td><strong>Author:</strong> </td><td><?php echo $data->author; ?></td>
                </tr>
                
                <tr>
                  <td><strong>Status:</strong> </td><td><?php if($data->status==1) echo 'Active'; else echo 'InActive'; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
