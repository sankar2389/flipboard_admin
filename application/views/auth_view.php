<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Sankar2389 | Admin Login</title>

    <!-- Bootstrap core CSS -->

  <link href="<?php echo site_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">

  <link href="<?php echo site_url('assets/fonts/css/font-awesome.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo site_url('assets/css/animate.min.css'); ?>" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="<?php echo site_url('assets/css/custom.css'); ?>" rel="stylesheet">
  <link href="<?php echo site_url('assets/css/icheck/flat/green.css'); ?>" rel="stylesheet">

  <script src="<?php echo site_url('assets/js/jquery.min.js'); ?>"></script>

  <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->  
        
   <link rel="icon" type="image/png" href="<?php echo site_url('assets/images/favicon-32x32.png'); ?>" sizes="32x32" />
   <link rel="icon" type="image/png" href="<?php echo site_url('assets/images/favicon-16x16.png'); ?>" sizes="16x16" />

</head>

<body style="background:#F7F7F7;">

  <div class="">
    <a class="hiddenanchor" id="toregister"></a>
    <a class="hiddenanchor" id="tologin"></a>

    <div id="wrapper">
      <div id="login" class="animate form">
        <section class="login_content">			  
   		  <?php echo form_open('verifylogin'); ?>
		   <h1> <img src="<?php echo site_url(''); ?>assets/images/logo1.png" style="width:32px; height:32px;" /> Sankar2389</h1>
			<div>
			  <input type="radio" name="user_type" id="user_type" value="admin" required="" /> Admin&nbsp;&nbsp;&nbsp;
                            <input type="radio" name="user_type" id="user_type" value="company" required="" /> Company&nbsp;&nbsp;&nbsp;
			  <input type="radio" name="user_type" id="user_type" value="user" required="" /> User<br/>
                      	
			</div>
			 <div class="clearfix"><br/></div>
			  <div class="clearfix"></div>
            <div>
              <input type="text" name="username" id="username" class="form-control" placeholder="Username" required="" />
            </div>
            <div>
              <input type="password" name="password" id="password" class="form-control" placeholder="Password" required="" />
            </div>
            <?php if ($this->session->flashdata('error_msg')) { ?>
			<div class="logerr"> <?= $this->session->flashdata('error_msg') ?> </div>
		    <?php } ?>
            <div>
			  <input type="submit" name="submit" class="btn btn-default submit" value="Log in" />
              <a class="reset_pass" href="#">Lost your password?</a>
            </div>
            <div class="clearfix"></div>
            <div class="separator">
              <div class="clearfix"></div>
              <br />
              <div>
               
              </div>
            </div>
          </form>
          <!-- form -->
        </section>
        <!-- content -->
      </div>
      
    </div>
  </div>

</body>

</html>
