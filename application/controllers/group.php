<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Group extends CI_Controller {
 
	public function __construct() 
	{
        parent:: __construct();		
		if($this->session->userdata('logged_in'))
		{
			$session_data = $this->session->userdata('logged_in');	 
			$this->username = $session_data['username'];	
			$this->user_id = $session_data['id'];	
			$this->user_type = $session_data['user_type'];	
       		$this->load->helper("url");
			$this->load->model("group_model");
        	$this->load->library("pagination");
		} else {
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
    }
	
	function index() 
	{
		show_404();
	}
	
	function lists()
	{
		$data['username'] = $this->username;
		$data['user_type'] = $this->user_type;
		$this->load->view('header', $data);
					
		$config = array();
		$config["base_url"] = base_url() . "group/lists";
		$config["total_rows"] = $this->group_model->record_count();
		$config["per_page"] = 20;
		$config["uri_segment"] = 3;

		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data["results"] = $this->group_model->fetch_groups($config["per_page"], $page);
		$data["links"] = $this->pagination->create_links();		
//echo "<pre>"; print_r($data);
		$this->load->view("group_view", $data);			
		$this->load->view('footer');		
	}
	
	function add() 
	{
		$data['username'] = $this->username;
		$data['user_type'] = $this->user_type;
		$data["allusers"] = $this->group_model->get_all_users();
        	if($this->input->post('submit'))
		{
			$groupExists = $this->group_model->isGroupExist($this->input->post('group_name')); 
			if($groupExists) {
				$this->session->set_flashdata('error_msg', 'This group is already added in the database');
				redirect('group/add');
			} else {
				$this->group_model->add_group();
				$this->session->set_flashdata('success_msg', 'Group added');
				redirect('group/lists');
			}			
		}
		
		$this->load->view('header', $data);
		$this->load->view("group_add", $data);			
		$this->load->view('footer');		
	}
	
	function edit() 
	{
		$data['username'] = $this->username;	
		$data['user_type'] = $this->user_type;	
		$id = $this->uri->segment(3);
		$data["results"] = $this->group_model->get_groups($id);
		$data["allusers"] = $this->group_model->get_all_users();
		
		if($this->input->post('submit'))
		{
			$groupExists = $this->group_model->isGroupExistInUpdate($this->input->post('group_name'), $id); 
			if($groupExists) {
				$this->session->set_flashdata('error_msg', 'This group is already added in the database');
				redirect('group/edit/'.$id);
			} else {
				$this->group_model->update_group($id);
				$this->session->set_flashdata('success_msg', 'Group updated');
				redirect('group/lists');
			}			
		}
		
		$this->load->view('header', $data); 
		$this->load->view("group_edit", $data);			
		$this->load->view('footer');		
	}
	
	function view()
	{
		$data['username'] = $this->username;
		$data['user_type'] = $this->user_type;
		$id = $this->uri->segment(3);
		$data["results"] = $this->group_model->get_groups($id);
		$this->load->view('header', $data);
		$this->load->view("group_viewbyid", $data);			
		$this->load->view('footer');		
	}
	
	function delete()
	{
		$id = $this->uri->segment(3);
		$this->group_model->delete_group($id);
		redirect('group/lists');	
	}
	
	function status() 
	{
		$status = $this->uri->segment(3);
		$id = $this->uri->segment(4);
		
		if($status=='active')
		$this->group_model->update_status($id, 1);
		else
		$this->group_model->update_status($id, 0);
		
		redirect('group/lists');	
	}

	function formsubmit()
	{
		$action = $this->input->post('action');
		$chkall = $this->input->post('chkall');

		if($action=='Inactive')
		{
			$ids = implode(',', $chkall);
			$this->group_model->chkall_inactive($ids, 0);
			$this->session->set_flashdata('error_msg', 'This group has been deactivated');
			redirect('group/lists'); 
		}

		if($action=='Active')
		{
			$ids = implode(',', $chkall);
			$this->group_model->chkall_active($ids, 1);
			$this->session->set_flashdata('success_msg', 'This group has been activated');
			redirect('group/lists'); 
		}

		if($action=='Delete')
		{
			$ids = implode(',', $chkall);
			$this->group_model->chkall_delete($ids, 1);
			$this->session->set_flashdata('error_msg', 'Group has been deleted');
			redirect('group/lists'); 
		}

	}
 
}
 
?>
