<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Webservice extends CI_Controller {
 
	public function __construct() 
	{
        parent:: __construct();		
		$this->load->helper("url");
		$this->load->model('webservice_model');	
    }
    
    function index() 
	{
		show_404();
	}
	
	function user_login()
	{
		$username = isset($_REQUEST['username'])?$_REQUEST['username']:'';
 		$password = isset($_REQUEST['password'])?$_REQUEST['password']:'';
		if($username!='' && $password!='') {				
			$data = array(
				'username' => $username,		
				'password' => $password		
			);	
			$res =$this->webservice_model->check_login($data); 						
			if($res) {
				foreach($res as $r) 
				$user_id = $r->user_id;
				
				$outp['result'] = 'success';
				$outp['data'] = array('message' => 'User Login Success', 
									  'id' => $user_id
								);	
				if($res)
				$outp['status'] = 1;
				else
				$outp['status'] = 0;
				echo json_encode($outp);
			}
			else 
				$this->json_msg('fail', 'Incorrect username or password');				
		} else 		
				$this->json_msg('fail', 'Please enter all required fields');		
	}
	
	function change_password()
	{
		$current_password = isset($_REQUEST['current_password'])?$_REQUEST['current_password']:'';
 		$confirm_password = isset($_REQUEST['confirm_password'])?$_REQUEST['confirm_password']:'';
 		$confirm_new_password = isset($_REQUEST['confirm_new_password'])?$_REQUEST['confirm_new_password']:'';
 		$user_id = isset($_REQUEST['id'])?$_REQUEST['id']:'';
		
		if($current_password!='' && $confirm_password!='' && $confirm_new_password!='') {	
			$res = $this->webservice_model->change_pass($current_password, $confirm_new_password, $user_id);
		} else 		
			$this->json_msg('fail', 'Please enter all required fields');		
	}
	
	function get_content()
	{
		 $this->webservice_model->get_content();
	}
	
	function get_content_byid()
	{
		$id = isset($_REQUEST['id'])?$_REQUEST['id']:'';
		if($id!='') {		
			$res =$this->webservice_model->check_login_byid($id);
			if($res==1)
				 $this->webservice_model->get_content_byid($id);
	 		else
				$this->json_msg('fail', 'Invalid user');
		} else 			
			$this->json_msg('fail', 'You Must Login to Continue');				
	}
	
	function get_content_byid_old()
	{
		$id = isset($_REQUEST['id'])?$_REQUEST['id']:'';
		if($id!='') {		
			$res =$this->webservice_model->check_login_byid($id);
			if($res==1)
				 $this->webservice_model->get_content_byid_old($id);
	 		else
				$this->json_msg('fail', 'Invalid user');
		} else 			
			$this->json_msg('fail', 'You Must Login to Continue');				
	}
	
	function json_msg($result, $msg) {
		$value['result'] = $result;
		$value['message'] = $msg;		
		$value['status'] = 0;		
		echo json_encode($value);			
	}

       function get_content_bookbyid()
	{ 
		$id = isset($_REQUEST['id'])?$_REQUEST['id']:'';
		if($id!='') {	
		$this->webservice_model->get_content_bookbyid($id);
		}
		else
		{
		$this->json_msg('fail', 'Enter valid book id  to Continue');	
		}
	}
 
}
 
?>