<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Edit_profile extends CI_Controller {
 
	public function __construct() 
	{
        parent:: __construct();		
		if($this->session->userdata('logged_in'))
		{
			$session_data = $this->session->userdata('logged_in');	
			$this->username = $session_data['username'];	
			$this->user_id = $session_data['id'];	
			$this->user_type = $session_data['user_type'];	
       		$this->load->helper("url");
			$this->load->model("user_model");
        	$this->load->library("pagination");
		} else {
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
    }
	
	function index() 
	{
		$data['user_id'] = $this->user_id;
		$data['username'] = $this->username;
		$data['user_type'] = $this->user_type; 
		$data['users'] = $this->user_model->get_user($this->user_id);
		$this->load->view('header', $data);
		$this->load->view('edit_profile', $data);
	}
	
	function save()
	{		
		if($this->input->post('submit'))
		{
			$userid = $this->user_id;
			$data = array(
						'username' => $this->input->post('username'),
						'name' => $this->input->post('name'),
						'email' => $this->input->post('email'),
						'department' => $this->input->post('department'),
						'designation' => $this->input->post('designation')
					);
			$this->user_model->edit_profile($data, $userid);
			$this->session->set_flashdata('success_msg', 'User profile updated');
			redirect('edit_profile');
		}	
	}
	
}
 
?>
