<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class All_groups extends CI_Controller {
 
	public function __construct() 
	{
        parent:: __construct();		
		if($this->session->userdata('logged_in'))
		{
			$session_data = $this->session->userdata('logged_in');	 
			$this->username = $session_data['username'];	
			$this->user_id = $session_data['id'];	
			$this->user_type = $session_data['user_type'];	
       		$this->load->helper("url");
			$this->load->model("all_group_model");
        	$this->load->library("pagination");
		} else {
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
    }
	
	function index() 
	{
		$data["sharegroup"] = $this->all_group_model->get_share_group($this->user_id);
		$data['username'] = $this->username;
		$data['user_type'] = $this->user_type;
		$data['user_id'] = $this->user_id;
		$this->load->view('header', $data);					
		$this->load->view("all_group_view", $data);			
		$this->load->view('footer');		
	}
	
	function share_content()
	{	
		if(isset($_POST['group'])) {
			$group = $_POST['group'];
			$imp = implode($group, ',');
		} else 
			$imp = '';
		
		$data = array(
		   'from' => $this->input->post('from') ,
		   'to' => $this->input->post('to') ,
		   'content_id' => $this->input->post('content_id') ,
		   'share_date' => date('Y-m-d') ,
		   'share_time' => date('h:i:s') ,
		   'group'	=> $imp
		);	
		
		$res = $this->all_group_model->share_content($data);
		$name = $this->all_group_model->get_username($this->input->get('to'));

		return true;
	}
 
}
 
?>
