<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class VerifyLogin extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->helper("url");
	}

	function index()
	{

          $user_type = $this->input->post('user_type');

             if($user_type!="" AND $user_type!="company")
             {
 		// This method will have the credentials validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
		if ($this->form_validation->run() == FALSE) {
			// Field validation failed.  User redirected to login page
			$this->load->view('auth_view');
		}
		else {			
			// Go to private area
			//redirect('home', 'refresh');
		}

          }
          else
          {
             	$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_databaseCompany');

               if ($this->form_validation->run() == FALSE) {

			// Field validation failed.  User redirected to login page
			$this->load->view('auth_view');
		}
		else {			
			
		}

          }

	}


        function check_databaseCompany($password)
	{
          $username = $this->input->post('username');
	  $user_type = $this->input->post('user_type');
          $result = $this->user_model->loginCompany($username, $password);
 
	if ($result) {

              foreach($result as $row) {
               		if($row->status==0) {

				$this->session->set_flashdata('error_msg', 'Sorry this user is not active');
				redirect('');
				return false;

		        }
                        elseif($row->first_time_login==0) 
                        {
                          $data = array('first_time_login' =>1);
			  $this->db->where('group_id', $row->group_id);
   			  $this->db->update('group', $data); 

				$sess_array = array(
				'id' => $row->group_id,
				'username' => $row->group_name,
				'user_type' => 'company',
				'first_time_login' => 'yes'
				);
				$this->session->set_userdata('logged_in', $sess_array);						
				redirect('change_pass_company', 'refresh');
                                   return true;

                        }
                        else
                        {
                              
                                $sess_array = array(
				'id' => $row->group_id,
				'username' => $row->group_name,
				'user_type' => 'company',
				'first_time_login' => 'no'

				);
				$this->session->set_userdata('logged_in', $sess_array);						
				redirect('mcontent/listsCompany', 'refresh');
                                   return true;


                        }
         


              }



	}
	else
	{
		$this->session->set_flashdata('error_msg', 'Invalid username or password');
		redirect('');
		return false;
	}


//exit;
//return false;
//echo $password;
        }

	function check_database($password)
	{
		// Field validation succeeded.  Validate against database
		$username = $this->input->post('username');
		$user_type = $this->input->post('user_type');

		// query the database
		$result = $this->user_model->login($username, $password);
		if ($result) {
			$sess_array = array();
			foreach($result as $row) {
				if($row->status==0) {
					$this->session->set_flashdata('error_msg', 'Sorry this user is not active');
					redirect('');
					return false;
				} 
				else if ($user_type == $row->user_type) {
					if($row->user_type=='user') {						
						$this->db->select('first_time_login');
						$this->db->from('user');
						$this->db->where('user_id', $row->user_id);		
						$query = $this->db->get();
						foreach($query->result() as $f)
						{
							if($f->first_time_login=='no') {
								$data = array(
								   'first_time_login' => 'yes'
								);								
								$this->db->where('user_id', $row->user_id);
								$this->db->update('user', $data); 
								$sess_array = array(
									'id' => $row->user_id,
									'username' => $row->username,
									'user_type' => $row->user_type,
									'first_time_login' => 'yes'
								);
								$this->session->set_userdata('logged_in', $sess_array);						
								redirect('change_pass', 'refresh');
								return TRUE;
							} else {
								$sess_array = array(
									'id' => $row->user_id,
									'username' => $row->username,
									'user_type' => $row->user_type,
									'first_time_login' => 'no'
								);
								$this->session->set_userdata('logged_in', $sess_array);				
								redirect('mcontent/lists', 'refresh');
								return TRUE;
							}							
						}
						
					} else {
						$sess_array = array(
							'id' => $row->user_id,
							'username' => $row->username,
							'user_type' => $row->user_type
						);
						$this->session->set_userdata('logged_in', $sess_array);
						redirect('home', 'refresh');
						return TRUE;
					}
					
				} else {
					$this->session->set_flashdata('error_msg', 'Invalid user type');
					redirect('');
					return false;
				}
			}
		}
		else {
			$this->session->set_flashdata('error_msg', 'Invalid username or password');
			redirect('');
			return false;
		}
	}
}

?>
