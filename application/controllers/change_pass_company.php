<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Change_pass_company extends CI_Controller {
 
	public function __construct() 
	{
        parent:: __construct();		
		if($this->session->userdata('logged_in'))
		{
			$session_data = $this->session->userdata('logged_in');	
			$this->username = $session_data['username'];	
			$this->user_id = $session_data['id'];	
			$this->user_type = $session_data['user_type'];	
       		$this->load->helper("url");
			$this->load->model("user_model");
        	$this->load->library("pagination");
		} else {
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
    }
	
	function index() 
	{
		$data['user_id'] = $this->user_id;
		$data['username'] = $this->username;
		$data['user_type'] = $this->user_type; 
		$data['users'] = $this->user_model->get_user($this->user_id);
		$this->load->view('header', $data);
		$this->load->view('change_pass_company', $data);
	}
	
	function save()
	{		
		if($this->input->post('submit'))
		{
			$userid = $this->user_id;
			$current_pass = $this->input->post('current_pass');
			$new_pass = $this->input->post('new_pass');
			$confirm_new_pass = $this->input->post('confirm_new_pass');
			$this->user_model->change_pass_company($current_pass, $new_pass, $userid); 

echo $userid.'ssssssssssssssssssssssssssssss';
		}	
	}
	
}
 
?>
