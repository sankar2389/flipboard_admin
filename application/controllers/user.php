<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
 
	public function __construct() 
	{
        parent:: __construct();		
		if($this->session->userdata('logged_in'))
		{
			$session_data = $this->session->userdata('logged_in');	
			$this->username = $session_data['username'];	
			$this->user_id = $session_data['id'];
			$this->user_type = $session_data['user_type'];	
			$this->load->model("user_model");
        	$this->load->library("pagination");
		} else {
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
    }
	
	function index() 
	{
		show_404();
	}
	
	function lists()
	{
		$data['username'] = $this->username;
		$data['user_type'] = $this->user_type;
		$this->load->view('header', $data);
					
		$config = array();
		$config["base_url"] = base_url() . "user/lists";
		$config["total_rows"] = $this->user_model->record_count();
		$config["per_page"] = 20;
		$config["uri_segment"] = 3;

		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data["results"] = $this->user_model->fetch_users($config["per_page"], $page);
		$data["links"] = $this->pagination->create_links();		

		$this->load->view("user_view", $data);			
		$this->load->view('footer');		
	}
	
	function add() 
	{
		$data['username'] = $this->username;
		$data['user_type'] = $this->user_type;
		$data["group"] = $this->user_model->get_group();
		if($this->input->post('submit'))
		{
			$emailExists = $this->user_model->isEmailExist($this->input->post('email')); 
			if($emailExists) {
				$this->session->set_flashdata('error_msg', 'Email is already exists');
				redirect('user/add');
			} else {
				$this->user_model->add_user();
				$this->session->set_flashdata('success_msg', 'User added');
				redirect('user/add');
			}			
		}
		
		$this->load->view('header', $data);
		$this->load->view("user_add", $data);			
		$this->load->view('footer');		
	}
	
	function edit() 
	{
		$data['username'] = $this->username;		
		$data['user_type'] = $this->user_type;
		$id = $this->uri->segment(3);
		$data["results"] = $this->user_model->get_user($id);
		$data["group"] = $this->user_model->get_group();
		
		if($this->input->post('submit'))
		{
			$emailExists = $this->user_model->isEmailExistInUpdate($this->input->post('email'), $id); 
			if($emailExists) {
				$this->session->set_flashdata('error_msg', 'Email is already exists');
				redirect('user/edit/'.$id);
			} else {
				$this->user_model->update_user($id);
				$this->session->set_flashdata('success_msg', 'User updated');
				redirect('user/edit/'.$id);
			}			
		}
		
		$this->load->view('header', $data); 
		$this->load->view("user_edit", $data);			
		$this->load->view('footer');		
	}
	
	function view()
	{
		$data['username'] = $this->username;
		$data['user_type'] = $this->user_type;
		$id = $this->uri->segment(3);
		$data["results"] = $this->user_model->get_user($id);
		$this->load->view('header', $data);
		$this->load->view("user_viewbyid", $data);			
		$this->load->view('footer');		
	}
	
	function delete()
	{
		$id = $this->uri->segment(3);
		$this->user_model->delete_user($id);
		redirect('user/lists');	
	}
	
	function status() 
	{
		$status = $this->uri->segment(3);
		$id = $this->uri->segment(4);
		
		if($status=='active')
		$this->user_model->update_status($id, 1);
		else
		$this->user_model->update_status($id, 0);
		
		redirect('user/lists');	
	}

	function formsubmit()
	{
		$action = $this->input->post('action');
		$chkall = $this->input->post('chkall');

		if($action=='Inactive')
		{
			$ids = implode(',', $chkall);
			$this->user_model->chkall_inactive($ids, 0);
			$this->session->set_flashdata('error_msg', 'This user has been deactivated');
			redirect('user/lists'); 
		}

		if($action=='Active')
		{
			$ids = implode(',', $chkall);
			$this->user_model->chkall_active($ids, 1);
			$this->session->set_flashdata('success_msg', 'This user has been activated');
			redirect('user/lists'); 
		}

		if($action=='Delete')
		{
			$ids = implode(',', $chkall);
			$this->user_model->chkall_delete($ids, 1);
			$this->session->set_flashdata('error_msg', 'User has been deleted');
			redirect('user/lists'); 
		}

	}
 
}
 
?>
