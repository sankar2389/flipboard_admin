<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mcontent extends CI_Controller { 

	public function __construct() 
	{
        parent:: __construct();		
		if($this->session->userdata('logged_in'))
		{
			$session_data = $this->session->userdata('logged_in');	
			$this->user_id = $session_data['id'];	
			$this->username = $session_data['username'];	
			$this->user_type = $session_data['user_type'];	
       		$this->load->helper('url');		
			$this->load->model("mcontent_model");
        	$this->load->library("pagination");
		} else {
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
    }
	
	function index() 
	{
		show_404();
	}
	
	function lists()
	{
		$data['username'] = $this->username;
		$data['user_type'] = $this->user_type;
		$this->load->view('header', $data);
					
		$config = array();
		$config["base_url"] = base_url() . "mcontent/lists";
		$config["total_rows"] = $this->mcontent_model->record_count();
		$config["per_page"] = 20;
		$config["uri_segment"] = 3;

		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data["results"] = $this->mcontent_model->fetch_content($config["per_page"], $page);
		$data["links"] = $this->pagination->create_links();		

		$this->load->view("mcontent_view", $data);			
		$this->load->view('footer');		
	}

       function listsCompany()
	{
		$data['username'] = $this->username;
		$data['user_type'] = $this->user_type;
		$this->load->view('header', $data);
					
		$config = array();
		$config["base_url"] = base_url() . "mcontent/listsCompany";
		$config["total_rows"] = $this->mcontent_model->record_count_comp();
		$config["per_page"] = 20;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data["results"] = $this->mcontent_model->fetch_content_company($config["per_page"], $page);
		$data["links"] = $this->pagination->create_links();		

		$this->load->view("mcontent_comp_view", $data);			
		$this->load->view('footer');		
	}

        function listsAdmin()
	{
		$data['username'] = $this->username;
		$data['user_type'] = $this->user_type;
		$this->load->view('header', $data);
					
		$config = array();
		$config["base_url"] = base_url() . "mcontent/listsAdmin";
		$config["total_rows"] = $this->mcontent_model->record_count_admin();
		$config["per_page"] = 20;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data["results"] = $this->mcontent_model->fetch_content_admin($config["per_page"], $page);
		$data["links"] = $this->pagination->create_links();		

		$this->load->view("mcontent_admin_view", $data);			
		$this->load->view('footer');		
	}
	
	function add()
	{ 
		$data['username'] = $this->username;
		$data['user_type'] = $this->user_type;
		$data["group"] = $this->mcontent_model->get_group();
		$data["users"] = $this->mcontent_model->get_users();
		
		if($this->input->post('submit'))
		{			
			$group = $_POST['group'];
			$group_imp = implode($group, ',');				
			$users = $_POST['users'];
			$users_imp = implode($users, ',');	

			$file=array(
			    'type'=>$this->input->post('type'),
				'title'=>$this->input->post('title'),
				'description'=>$this->input->post('description'),
				'post_url'=>$this->input->post('post_url'),
				'author'=>$this->input->post('author'),
				'status'=>1,
				'share_group_id'=> $group_imp,
				'share_user_id'=> $users_imp,
				'login_user_id'=> $this->user_id
			);
			
			$titleExists = $this->mcontent_model->isTitleExist($this->input->post('title')); 
			if($titleExists) {
				$this->session->set_flashdata('error_msg', 'Title is already exists');
				redirect('mcontent/add');
			} else {
				$this->mcontent_model->add_content($file);
				$this->session->set_flashdata('success_msg', 'Content added');
				redirect('mcontent/add');
			}	

		}
		else
		{
			$this->load->view('header', $data);
			$this->load->view("mcontent_add", $data);			
			$this->load->view('footer');		
		}			
	}
        function addcomp()
	{ 
		$data['username'] = $this->username;
		$data['user_type'] = $this->user_type;
		
		if($this->input->post('submit'))
		{	
			
	                 $getUsers=$this->mcontent_model->get_usersComp();
                       
			$file=array(
			    'type'=>$this->input->post('type'),
				'title'=>$this->input->post('title'),
				'description'=>$this->input->post('description'),
				'post_url'=>$this->input->post('post_url'),
				'author'=>$this->input->post('author'),
				'status'=>1,
				'share_group_id'=> $this->user_id,
				'share_user_id'=> $getUsers,
				'login_user_id'=> $this->user_id
			);
			
			$titleExists = $this->mcontent_model->isTitleExistComp($this->input->post('title')); 
			if($titleExists) {
				$this->session->set_flashdata('error_msg', 'Title is already exists');
				redirect('mcontent/addcomp');
			} else {
				$this->mcontent_model->add_content_company($file);
				$this->session->set_flashdata('success_msg', 'Content added');
				redirect('mcontent/addcomp');
			}
                     	

		}
		else
		{
			$this->load->view('header', $data);
			$this->load->view("mcontent_comp_add", $data);			
			$this->load->view('footer');		
		}			
	}


function addadmin()
	{ 
		$data['username'] = $this->username;
		$data['user_type'] = $this->user_type;
		
		if($this->input->post('submit'))
		{	
			
	                 $getUsers=$this->mcontent_model->get_comp_admin();
                       
			$file=array(
			    'type'=>$this->input->post('type'),
				'title'=>$this->input->post('title'),
				'description'=>$this->input->post('description'),
				'post_url'=>$this->input->post('post_url'),
				'author'=>$this->input->post('author'),
				'status'=>1,
				'share_group_id'=> $getUsers,
				'share_user_id'=> $getUsers,
				'login_user_id'=> $this->user_id
			);
			
			$titleExists = $this->mcontent_model->isTitleExistAdmin($this->input->post('title')); 
			if($titleExists) {
				$this->session->set_flashdata('error_msg', 'Title is already exists');
				redirect('mcontent/addadmin');
			} else {
				$this->mcontent_model->add_content_admin($file);
				$this->session->set_flashdata('success_msg', 'Content added');
				redirect('mcontent/addadmin');
			}
                     	

		}
		else
		{
			$this->load->view('header', $data);
			$this->load->view("mcontent_admin_add", $data);			
			$this->load->view('footer');		
		}			
	}
	
	
	function edit()
	{ 
		$data['username'] = $this->username;
		$data['user_type'] = $this->user_type;
		$id = $this->uri->segment(3);
		$data['content'] = $this->mcontent_model->get_content($id);
		$data["group"] = $this->mcontent_model->get_group();
		$data["users"] = $this->mcontent_model->get_users();
		 		
		if($this->input->post('submit'))
		{
			$group = $_POST['group'];
			$group_imp = implode($group, ',');			
			$users = $_POST['users'];
			$users_imp = implode($users, ',');	

			$file=array(
				'type'=>$this->input->post('type'),
				'title'=>$this->input->post('title'),
				'description'=>$this->input->post('description'),
				'post_url'=>$this->input->post('post_url'),
				'author'=>$this->input->post('author'),
				'status'=>1,
				'share_group_id'=> $group_imp,
				'share_user_id'=> $users_imp,
				'login_user_id'=> $this->user_id
			);
			
			$titleExists = $this->mcontent_model->isTitleExistInUpdate($this->input->post('title'), $id); 
			if($titleExists) {
				$this->session->set_flashdata('error_msg', 'Title is already exists');
				redirect('mcontent/edit/'.$id);
			} else {
				$this->mcontent_model->update_content($file, $id);
				$this->session->set_flashdata('success_msg', 'Content updated');
				redirect('mcontent/edit/'.$id);
			}	
		}
		else
		{
			$this->load->view('header', $data);
			$this->load->view("mcontent_edit", $data);			
			$this->load->view('footer');		
		}			
	}

function edit_comp()
	{ 
		$data['username'] = $this->username;
		$data['user_type'] = $this->user_type;
		$id = $this->uri->segment(3);
		$data['content'] = $this->mcontent_model->get_content_comp($id);
		$data["group"] = $this->mcontent_model->get_group();
		$data["users"] = $this->mcontent_model->get_usersComp();
		  	
		if($this->input->post('submit'))
		{
			$group = $_POST['group'];
			$group_imp = implode($group, ',');			
			$users = $_POST['users'];
			$users_imp = implode($users, ',');	
                        $getUsers=$this->mcontent_model->get_usersComp();	
			$file=array(
				'type'=>$this->input->post('type'),
				'title'=>$this->input->post('title'),
				'description'=>$this->input->post('description'),
				'post_url'=>$this->input->post('post_url'),
				'author'=>$this->input->post('author'),
				'status'=>1,
				'share_group_id'=> $this->user_id,
				'share_user_id'=> $getUsers,
				'login_user_id'=> $this->user_id
			);
			
			$titleExists = $this->mcontent_model->isTitleExistInUpdate_comp($this->input->post('title'), $id); 
			if($titleExists) {
				$this->session->set_flashdata('error_msg', 'Title is already exists');
				redirect('mcontent/edit_comp/'.$id);
			} else {
				$this->mcontent_model->update_content_comp($file, $id);
				$this->session->set_flashdata('success_msg', 'Content updated');
				redirect('mcontent/edit_comp/'.$id);
			}	
		}
		else
		{
			$this->load->view('header', $data);
			$this->load->view("mcontent_comp_edit", $data);			
			$this->load->view('footer');		
		}			
	}	



function edit_admin()
	{ 
		$data['username'] = $this->username;
		$data['user_type'] = $this->user_type;
		$id = $this->uri->segment(3);
		$data['content'] = $this->mcontent_model->get_content_admin($id);
		$getUsers=$this->mcontent_model->get_comp_admin(); 
		if($this->input->post('submit'))
		{ 
			
			$file=array(
				'type'=>$this->input->post('type'),
				'title'=>$this->input->post('title'),
				'description'=>$this->input->post('description'),
				'post_url'=>$this->input->post('post_url'),
				'author'=>$this->input->post('author'),
				'status'=>1,
				'share_group_id'=> $getUsers,
				'share_user_id'=> $getUsers,
				'login_user_id'=> $this->user_id
			);
			
			$titleExists = $this->mcontent_model->isTitleExistInUpdate_admin($this->input->post('title'), $id); 

			if($titleExists) {
				$this->session->set_flashdata('error_msg', 'Title is already exists');
				redirect('mcontent/edit_admin/'.$id);
			} else {
				$this->mcontent_model->update_content_admin($file, $id);
				$this->session->set_flashdata('success_msg', 'Content updated');
				redirect('mcontent/edit_admin/'.$id);
			}	
		}
		else
		{
			$this->load->view('header', $data);
			$this->load->view("mcontent_admin_edit", $data);			
			$this->load->view('footer');		
		}			
	}	
	

	function delete()
	{
		$id = $this->uri->segment(3);
		$data['image'] = $this->mcontent_model->get_image($id); 
	
		foreach($data['image'] as $b)
		{
			$img_name = $b->image;
			@unlink('./uploads/mcontent/'.$img_name);
		}

		$this->mcontent_model->delete_content($id);
		redirect('mcontent/lists');
	}


     function delete_comp()
	{
		$id = $this->uri->segment(3);
		$data['image'] = $this->mcontent_model->get_image_comp($id); 
	
		foreach($data['image'] as $b)
		{
			$img_name = $b->image;
			@unlink('./uploads/mcontent_company/'.$img_name);
		}

		$this->mcontent_model->delete_content_comp($id);
		redirect('mcontent/listsCompany');
	}

  function delete_admin() //need work by tomorrow
	{
		$id = $this->uri->segment(3);
		$data['image'] = $this->mcontent_model->get_image_admin($id); 
	
		foreach($data['image'] as $b)
		{
			$img_name = $b->image;
			@unlink('./uploads/mcontent/'.$img_name);
		}

		$this->mcontent_model->delete_content_admin($id);
		redirect('mcontent/listsAdmin');
	}
	
	function view()
	{
		$data['username'] = $this->username;
		$data['user_type'] = $this->user_type;
		$id = $this->uri->segment(3);
		$data["results"] = $this->mcontent_model->get_content($id);
		$this->load->view('header', $data);
		$this->load->view("mcontent_viewbyid", $data);			
		$this->load->view('footer');		
	}

function view_comp()
	{
		$data['username'] = $this->username;
		$data['user_type'] = $this->user_type;
		$id = $this->uri->segment(3);
		$data["results"] = $this->mcontent_model->get_content_comp($id);
		$this->load->view('header', $data);
		$this->load->view("mcontent_comp_viewbyid", $data);			
		$this->load->view('footer');		
	}
function view_admin()
	{
		$data['username'] = $this->username;
		$data['user_type'] = $this->user_type;
		$id = $this->uri->segment(3);
		$data["results"] = $this->mcontent_model->get_content_admin($id);

//print_r($data["results"]);

		$this->load->view('header', $data);
		$this->load->view("mcontent_admin_viewbyid", $data);			
		$this->load->view('footer');		
	}

	
	function status() 
	{
		$status = $this->uri->segment(3);
		$id = $this->uri->segment(4);
		
		if($status=='active')
		$this->mcontent_model->update_status($id, 1);
		else
		$this->mcontent_model->update_status($id, 0);
		
		redirect('mcontent/lists');	
	}

       function status_comp() 
	{
		$status = $this->uri->segment(3);
		$id = $this->uri->segment(4);
		
		if($status=='active')
		$this->mcontent_model->update_status_comp($id, 1);
		else
		$this->mcontent_model->update_status_comp($id, 0);
		
		redirect('mcontent/listsCompany');	
	}
	
      function status_admin() 
	{
		$status = $this->uri->segment(3);
		$id = $this->uri->segment(4);
		
		if($status=='active')
		$this->mcontent_model->update_status_admin($id, 1);
		else
		$this->mcontent_model->update_status_admin($id, 0);
		
		redirect('mcontent/listsAdmin');	
	}

	function delete_image()
	{
		$id = $this->input->post('id');
		$data['image'] = $this->mcontent_model->get_preview_image($id); 		
		foreach($data['image'] as $b)
		{
			$img_name = $b->image;
			unlink('./uploads/mcontent/'.$img_name);
		}
		$res = $this->mcontent_model->delete_image($id);	
		redirect('mcontent/lists');		
		
	}

      function delete_comp_image()
	{
		$id = $this->input->post('id');
		$data['image'] = $this->mcontent_model->get_preview_image_comp($id); 		
		foreach($data['image'] as $b)
		{
			$img_name = $b->image;
			unlink('./uploads/mcontent_company/'.$img_name);
		}
		$res = $this->mcontent_model->delete_image_comp($id);	
		redirect('mcontent/listsCompany');		
		
	}

function delete_admin_image()
	{
		$id = $this->input->post('id');
		$data['image'] = $this->mcontent_model->get_preview_image_admin($id); 		
		foreach($data['image'] as $b)
		{
			$img_name = $b->image;
			unlink('./uploads/mcontent/'.$img_name);
		}
		$res = $this->mcontent_model->delete_image_admin($id);	
		redirect('mcontent/listsCompany');		
		
	}

	function formsubmit()
	{
		$action = $this->input->post('action');
		$chkall = $this->input->post('chkall');

		if($action=='Inactive')
		{
			$ids = implode(',', $chkall);
			$this->mcontent_model->chkall_inactive($ids, 0);
			$this->session->set_flashdata('error_msg', 'This contents has been deactivated');
			redirect('mcontent/lists'); 
		}

		if($action=='Active')
		{
			$ids = implode(',', $chkall);
			$this->mcontent_model->chkall_active($ids, 1);
			$this->session->set_flashdata('success_msg', 'This contents has been activated');
			redirect('mcontent/lists'); 
		}

		if($action=='Delete')
		{
			$ids = implode(',', $chkall);
			$this->mcontent_model->chkall_delete($ids, 1);
			$this->session->set_flashdata('error_msg', 'This contents has been deleted');
			redirect('mcontent/lists'); 
		}

	}


function formsubmitComp()
	{
		$action = $this->input->post('action');
		$chkall = $this->input->post('chkall');

		if($action=='Inactive')
		{
			$ids = implode(',', $chkall);
			$this->mcontent_model->chkall_inactive_comp($ids, 0);
			$this->session->set_flashdata('error_msg', 'This contents has been deactivated');
			redirect('mcontent/listsCompany'); 
		}

		if($action=='Active')
		{
			$ids = implode(',', $chkall);
			$this->mcontent_model->chkall_active_comp($ids, 1);
			$this->session->set_flashdata('success_msg', 'This contents has been activated');
			redirect('mcontent/listsCompany'); 
		}

		if($action=='Delete')
		{
			$ids = implode(',', $chkall);
			$this->mcontent_model->chkall_delete_comp($ids, 1);
			$this->session->set_flashdata('error_msg', 'This contents has been deleted');
			redirect('mcontent/listsCompany'); 
		}

	}


function formsubmitAdmin()
	{
		$action = $this->input->post('action');
		$chkall = $this->input->post('chkall');

		if($action=='Inactive')
		{
			$ids = implode(',', $chkall);
			$this->mcontent_model->chkall_inactive_admin($ids, 0);
			$this->session->set_flashdata('error_msg', 'This contents has been deactivated');
			redirect('mcontent/listsAdmin'); 
		}

		if($action=='Active')
		{
			$ids = implode(',', $chkall);
			$this->mcontent_model->chkall_active_admin($ids, 1);
			$this->session->set_flashdata('success_msg', 'This contents has been activated');
			redirect('mcontent/listsAdmin'); 
		}

		if($action=='Delete')
		{
			$ids = implode(',', $chkall);
			$this->mcontent_model->chkall_delete_admin($ids, 1);
			$this->session->set_flashdata('error_msg', 'This contents has been deleted');
			redirect('mcontent/listsAdmin'); 
		}

	}

	function user_notin_group()
	{
		$gIds = '0,';
		$gIds .= $this->input->post('gIDs');
		$sql = "SELECT * FROM `user` WHERE `group` NOT IN($gIds)";
		$query = $this->db->query($sql);

        $select = '<strong>Users</strong><br/>';
		$select .= '<select name="users[]" id="users" class="form-control" size="10" required multiple>';
		foreach($query->result() as $res)
		{
			$select .= '<option value='.$res->user_id.'>'.$res->name.'</option>';
		}
		$select .= '</select>';
		$select .= '<span style="color:blue;font-size:11px;">Press CTRL + Select to choose multiple users</span><br/><br/>';

		echo $select;
	}

}
