<?php
Class All_group_model extends CI_Model
{
	public function __construct() {
		parent::__construct();
	}

	function get_group()
    {
		$this->db->select('*');
		$this->db->from('group');
        $query = $this->db->get();
        return $query->result();
    }	
    
    function get_users($uid)
    {
		$this->db->select('*');
		$this->db->from('mcontent');
		$this->db->where('user_id != '.$uid.'');
        $query = $this->db->get();
        return $query->result();
    }	
    
    function get_username($uid){ 
		$this->db->select('name');
		$this->db->from('user');
		$this->db->where('user_id',$uid); 
		return $this->db->get()->row()->name;
	}
	
	function share_content($data)
	{
		$this->db->insert('share_content', $data); 
	}
	
	function disable_content($userid, $content_id)
	{
		$this->db->select('*');
		$this->db->from('share_content');
		$this->db->where('to',$userid);
		$this->db->where('content_id',$content_id);
        $query = $this->db->get();
        return $query->result();
	}
	
		
	function get_share_group($uid=NULL)
    {
		$this->db->select('share_group_id');
		$this->db->from('mcontent');
		$this->db->where('login_user_id', $uid);
        $query = $this->db->get();
        return $query->result();
    }
    
    function groupname_byid($gid){ 
		$this->db->select('group_name');
		$this->db->from('group');
		$this->db->where('group_id',$gid); 
		return $this->db->get()->row()->group_name;
	}
	
	 function get_content_bygroup($gid, $uid){ 
		$this->db->select('*');
		$this->db->from('mcontent');
		$this->db->like('share_group_id',$gid); 
		$this->db->where('login_user_id', $uid);
		$query = $this->db->get();
        return $query->result();
	}
	
	 function username_byid($uids){ 
		$sql = "SELECT name FROM user WHERE user_id IN (".$uids.")"; 
		$query = $this->db->query($sql);
		$name = '';
        foreach( $query->result() as $q) {
			$name .= $q->name.'<br/>';
		}
		return $name;
	}
	
}
?>
