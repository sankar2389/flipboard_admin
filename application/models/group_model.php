<?php
Class Group_model extends CI_Model
{
	public function __construct() {
		parent::__construct();
	}
	
	public function record_count() {
		$txt_search = $this->input->post('txt_search');
		if($txt_search!='') {
			$st="(group_name LIKE '%".$txt_search."%')";
  			$this->db->where($st);  
		}
		$this->db->order_by("group_id", "desc");		
		$query = $this->db->get("group");
		return $query->num_rows();
	}
	
	public function fetch_groups($limit, $start) {
		$this->db->limit($limit, $start);	
		$txt_search = $this->input->post('txt_search');
		if($txt_search!='') {		
			$st="(group_name LIKE '%".$txt_search."%')";
  			$this->db->where($st);  
		}
		$this->db->order_by("group_id", "desc");	
		$query = $this->db->get("group");
		//echo $this->db->last_query();
	
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		} else {
			$data = 0;
			return $data;
		}		
	}
	   
    function get_group()
    {
		$this->db->select('*');
		$this->db->from('group');
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_all_users()
    {
                $this->db->select('group_concat(users) as a ');
		$this->db->from('group');
		$this->db->where('users !=""');
//$this->db->where('users ="ddddd"');
		$queryIn = $this->db->get();
 		$k=$queryIn->result() ;
             if(isset($k[0]->a) AND $k[0]->a !="")
              {
                $NotInId=$k[0]->a;
                $this->db->select('*');
		$this->db->from('user');
		$this->db->where('user_type !="admin"');
                $this->db->where('user_id not in('.$NotInId.')');
		$this->db->where('status', 1);
                $query = $this->db->get();
               return $query->result();
              }
              else{
                $this->db->select('*');
		$this->db->from('user');
		$this->db->where('user_type !="admin"');
                 
		$this->db->where('status', 1);
                $query = $this->db->get();
              return $query->result();

         }


/*
		$gg='';//$k[0]->a;
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('user_type !="admin"');
                $this->db->where('user_id not in('.$gg.')');
		$this->db->where('status', 1);
        $query = $this->db->get();
        return $query->result();*/
    }
	
	function get_features_byid($userid)
    {
		$this->db->select('*');
		$this->db->from('menu_ref');
		$this->db->join('menu', 'menu_ref.menu_id = menu.menu_id', 'left');
		$this->db->where('menu_ref.user_id', $userid);
		
		$query = $this->db->get();
        return $query->result();
    }

        function random_company_password( $length = 8 ) {
    	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    	$password = substr( str_shuffle( $chars ), 0, $length );
    	return $password;
	}
	
	function add_group()
	{	

$password = $this->random_company_password();

		$data = array(
		   'group_name' => $this->input->post('group_name') ,
		   'status' => 1,
                   'password' => $password  
		);
		
		$this->db->insert('group', $data); 				 
		$gid = $this->db->insert_id();	
		$users = $this->input->post('users');
		if(!empty($users))
		{
			$imp = implode($users, ','); 
			$value = array('users' => $imp);			
			$this->db->where('group_id', $gid);
			$this->db->update('group', $value); 
		}
	
		return true;				
	}
	
	function update_group($id)
	{						
		$data = array(
		   'group_name' => $this->input->post('group_name') 
		);
		
		//clear old group value
		$value = array('users' => '');			
		$this->db->where('group_id', $id);
		$this->db->update('group', $value); 
		
		//update new selected users value
		$users = $this->input->post('users');
		if(!empty($users))
		{
			$imp = implode($users, ','); 
			$value = array('users' => $imp);			
			$this->db->where('group_id', $id);
			$this->db->update('group', $value); 
		}
		
		$this->db->where('group_id', $id);
		$this->db->update('group', $data); 		

		return true;				
	}
	
	function update_status($id, $status)
	{
		$data = array(
		   'status' => $status
		);
		
		$this->db->where('group_id', $id);
		$this->db->update('group', $data); 
	}
	
	function delete_group($id)
	{
		$this->db->delete('group', array('group_id' => $id)); 		
	}	

	function isGroupExist($gname) {
		$this->db->select('group_id');
		$this->db->where('group_name', $gname);
		$query = $this->db->get('group');

		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	function isGroupExistInUpdate($gname, $id) {
		$this->db->select('group_id');
		$this->db->where('group_name', $gname);
		$this->db->where('group_id != '.$id.'');
		$query = $this->db->get('group');

		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	function get_groups($id = 0) 
	{
		$this->db->select('*');
        $this->db->where('group_id', $id);
        $query = $this->db->get('group');		
        return $query->result();
	}
	
	function get_group_byid($gid){ 
		$this->db->select('group_name');
		$this->db->from('group');
		$this->db->where('group_id',$gid); 
		return $this->db->get()->row()->group_name;
	}
	
	function get_user_byid($uid){ 
		$this->db->select('name');
		$this->db->from('user');
		$this->db->where('user_id',$uid); 
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			foreach ($query->result() as $r)
			echo $r->name;
		} else {
			echo '---';
		}
	}

	function chkall_inactive($ids, $status)
	{
		$sql  = 'UPDATE `group` SET `status` = '.$status.' WHERE `group_id` IN ('.$ids.')';
		$update = $this->db->query($sql);
	}

	function chkall_active($ids, $status)
	{
		$sql  = 'UPDATE `group` SET `status` = '.$status.' WHERE `group_id` IN ('.$ids.')';
		$update = $this->db->query($sql);
	}

	function chkall_delete($ids, $status)
	{
		$sql  = 'DELETE FROM `group` WHERE `group_id` IN ('.$ids.')';
		$update = $this->db->query($sql);
	}
	
}
?>
