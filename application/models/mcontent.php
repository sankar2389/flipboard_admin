<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mcontent extends CI_Controller { 

	public function __construct() 
	{
        parent:: __construct();		
		if($this->session->userdata('logged_in'))
		{
			$session_data = $this->session->userdata('logged_in');	
			$this->user_id = $session_data['id'];	
			$this->username = $session_data['username'];	
			$this->user_type = $session_data['user_type'];	
       		$this->load->helper('url');		
			$this->load->model("mcontent_model");
        	$this->load->library("pagination");
		} else {
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
    }
	
	function index() 
	{
		show_404();
	}
	
	function lists()
	{
		$data['username'] = $this->username;
		$data['user_type'] = $this->user_type;
		$this->load->view('header', $data);
					
		$config = array();
		$config["base_url"] = base_url() . "mcontent/lists";
		$config["total_rows"] = $this->mcontent_model->record_count();
		$config["per_page"] = 20;
		$config["uri_segment"] = 3;

		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data["results"] = $this->mcontent_model->fetch_content($config["per_page"], $page);
		$data["links"] = $this->pagination->create_links();		

		$this->load->view("mcontent_view", $data);			
		$this->load->view('footer');		
	}
	
	function add()
	{ 
		$data['username'] = $this->username;
		$data['user_type'] = $this->user_type;
		$data["group"] = $this->mcontent_model->get_group();
		$data["users"] = $this->mcontent_model->get_users();
		
		if($this->input->post('submit'))
		{			
			$group = $_POST['group'];
			$group_imp = implode($group, ',');				
			$users = $_POST['users'];
			$users_imp = implode($users, ',');	

			$file=array(
			    'type'=>$this->input->post('type'),
				'title'=>$this->input->post('title'),
				'description'=>$this->input->post('description'),
				'status'=>1,
				'share_group_id'=> $group_imp,
				'share_user_id'=> $users_imp,
				'login_user_id'=> $this->user_id
			);
			
			$titleExists = $this->mcontent_model->isTitleExist($this->input->post('title')); 
			if($titleExists) {
				$this->session->set_flashdata('error_msg', 'Title is already exists');
				redirect('mcontent/add');
			} else {
				$this->mcontent_model->add_content($file);
				$this->session->set_flashdata('success_msg', 'Content added');
				redirect('mcontent/add');
			}	

		}
		else
		{
			$this->load->view('header', $data);
			$this->load->view("mcontent_add", $data);			
			$this->load->view('footer');		
		}			
	}
	
	function edit()
	{ 
		$data['username'] = $this->username;
		$data['user_type'] = $this->user_type;
		$id = $this->uri->segment(3);
		$data['content'] = $this->mcontent_model->get_content($id);
		$data["group"] = $this->mcontent_model->get_group();
		$data["users"] = $this->mcontent_model->get_users();
		 		
		if($this->input->post('submit'))
		{
			$group = $_POST['group'];
			$group_imp = implode($group, ',');			
			$users = $_POST['users'];
			$users_imp = implode($users, ',');			

			$file=array(
				'type'=>$this->input->post('type'),
				'title'=>$this->input->post('title'),
				'description'=>$this->input->post('description'),
				'status'=>1,
				'share_group_id'=> $group_imp,
				'share_user_id'=> $users_imp,
				'login_user_id'=> $this->user_id
			);
			
			$titleExists = $this->mcontent_model->isTitleExistInUpdate($this->input->post('title'), $id); 
			if($titleExists) {
				$this->session->set_flashdata('error_msg', 'Title is already exists');
				redirect('mcontent/edit/'.$id);
			} else {
				$this->mcontent_model->update_content($file, $id);
				$this->session->set_flashdata('success_msg', 'Content updated');
				redirect('mcontent/edit/'.$id);
			}	
		}
		else
		{
			$this->load->view('header', $data);
			$this->load->view("mcontent_edit", $data);			
			$this->load->view('footer');		
		}			
	}	

	function delete()
	{
		$id = $this->uri->segment(3);
		$data['image'] = $this->mcontent_model->get_image($id); 		
		//remove image in folder
		foreach($data['image'] as $b)
		{
			$img_name = $b->image;
			unlink('./uploads/mcontent/'.$img_name);
		}
		$this->mcontent_model->delete_content($id);
		redirect('mcontent/lists');
	}
	
	function view()
	{
		$data['username'] = $this->username;
		$data['user_type'] = $this->user_type;
		$id = $this->uri->segment(3);
		$data["results"] = $this->mcontent_model->get_content($id);
		$this->load->view('header', $data);
		$this->load->view("mcontent_viewbyid", $data);			
		$this->load->view('footer');		
	}
	
	function status() 
	{
		$status = $this->uri->segment(3);
		$id = $this->uri->segment(4);
		
		if($status=='active')
		$this->mcontent_model->update_status($id, 1);
		else
		$this->mcontent_model->update_status($id, 0);
		
		redirect('mcontent/lists');	
	}

}
