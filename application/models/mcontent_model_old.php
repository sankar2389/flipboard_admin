<?php
Class Mcontent_model extends CI_Model
{
	public function __construct() {
		parent::__construct();
	}
	
	public function record_count() {
		$txt_search = $this->input->post('txt_search');
		if($txt_search!='') {	
			$this->db->like('title', $txt_search);
			$this->db->or_like('description', $txt_search);
		}
		$session_data = $this->session->userdata('logged_in');	
		$user_id = $session_data['id'];	
		$this->db->where('login_user_id', $user_id);
		$this->db->order_by("mc_id", "desc");	
		$query = $this->db->get("mcontent");
		return $query->num_rows();
	}

        public function record_count_comp() {
		$txt_search = $this->input->post('txt_search');
		if($txt_search!='') {	
			$this->db->like('title', $txt_search);
			$this->db->or_like('description', $txt_search);
		}
		$session_data = $this->session->userdata('logged_in');	
		$user_id = $session_data['id'];	
		$this->db->where('login_user_id', $user_id);
		$this->db->order_by("mc_id", "desc");	
		$query = $this->db->get("mcontent_company");
		return $query->num_rows();
	}

public function record_count_admin() {
		$txt_search = $this->input->post('txt_search');
		if($txt_search!='') {	
			$this->db->like('title', $txt_search);
			$this->db->or_like('description', $txt_search);
		}
		$session_data = $this->session->userdata('logged_in');	
		$user_id = $session_data['id'];	
		$this->db->where('login_user_id', $user_id);
		$this->db->order_by("mc_id", "desc");	
		$query = $this->db->get("mcontent");
		return $query->num_rows();
	}

 
	
	public function fetch_content($limit, $start) {
		$this->db->limit($limit, $start);	
		$txt_search = $this->input->post('txt_search');
		if($txt_search!='') {		
			$this->db->like('title', $txt_search);
			$this->db->or_like('description', $txt_search);
		}
		$session_data = $this->session->userdata('logged_in');	
		$user_id = $session_data['id'];
		$this->db->where('login_user_id', $user_id);
		$this->db->order_by("mc_id", "desc");
		$query = $this->db->get("mcontent");
		//echo $this->db->last_query();
	
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		} else {
			$data = 0;
			return $data;
		}		
	}


public function fetch_content_company($limit, $start) {
		$this->db->limit($limit, $start);	
		$txt_search = $this->input->post('txt_search');
		if($txt_search!='') {		
			$this->db->like('title', $txt_search);
			$this->db->or_like('description', $txt_search);
		}
		$session_data = $this->session->userdata('logged_in');	
		$user_id = $session_data['id'];
		$this->db->where('login_user_id', $user_id);
		$this->db->order_by("mc_id", "desc");
		$query = $this->db->get("mcontent_company");
		//echo $this->db->last_query();
	
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		} else {
			$data = 0;
			return $data;
		}		
	}


public function fetch_content_admin($limit, $start) {
		$this->db->limit($limit, $start);	
		$txt_search = $this->input->post('txt_search');
		if($txt_search!='') {		
			$this->db->like('title', $txt_search);
			$this->db->or_like('description', $txt_search);
		}
		$session_data = $this->session->userdata('logged_in');	
		$user_id = $session_data['id'];
		$this->db->where('login_user_id', $user_id);
		$this->db->order_by("mc_id", "desc");
		$query = $this->db->get("mcontent");
		//echo $this->db->last_query();
	
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		} else {
			$data = 0;
			return $data;
		}		
	}


	
	function get_content($mc_id)
    {
		$this->db->select('*');
		$this->db->from('mcontent');
		$this->db->where('mc_id', $mc_id);		
		$query = $this->db->get();
        return $query->result();
    }

function get_content_comp($mc_id)
    {
		$this->db->select('*');
		$this->db->from('mcontent_company');
		$this->db->where('mc_id', $mc_id);		
		$query = $this->db->get();
        return $query->result();
    }
	
function get_content_admin($mc_id)
    {
		$this->db->select('*');
		$this->db->from('mcontent');
		$this->db->where('mc_id', $mc_id);		
		$query = $this->db->get();
        return $query->result();
    }

	function add_content($data)
	{
		$this->db->insert('mcontent', $data); 
		$last_insert_id = $this->db->insert_id();
		$time = time();
		
		$upimage = $_FILES['upimage']['name'];
		$upimage_title = $_REQUEST['upimage_title'];		
		
		foreach($upimage as $u)
		$x[] = $time.$u;
		
		$combine = array_combine($x, $upimage_title);
	
		if($_FILES['upimage']['name'][0]!='')
		{
			foreach($combine as $key => $val)
			{
				$dt = array('content_id' => $last_insert_id, 'title' => $val, 'image' => $key);
				$this->db->insert('assign_image', $dt); 
			}	
		}	
		
		if($_FILES['upimage']['name'][0]!='') { 
			foreach($_FILES['upimage']['tmp_name'] as $key => $tmp_name)
			{
				if($tmp_name) {
					$file_name = $_FILES['upimage']['name'][$key];
					$file_size =$_FILES['upimage']['size'][$key];
					$file_tmp =$_FILES['upimage']['tmp_name'][$key];
					$file_type=$_FILES['upimage']['type'][$key];  
					$fname = $time.$file_name;
					$dir = "./uploads/mcontent/";
					move_uploaded_file($tmp_name, $dir.$fname);				
				}
			}
		}
		
		if(!empty($_REQUEST['upvideo']))
		{
			foreach($_REQUEST['upvideo'] as $key => $val)
			{
				if($val) {
					$dt = array('content_id' => $last_insert_id, 'video_url' => $val);
					$this->db->insert('assign_video_url', $dt); 
				}
			}
		}
	}

        function add_content_company($data)
	{
		$this->db->insert('mcontent_company', $data); 
		$last_insert_id = $this->db->insert_id();
		$time = time();
		
		$upimage = $_FILES['upimage']['name'];
		$upimage_title = $_REQUEST['upimage_title'];		
		
		foreach($upimage as $u)
		$x[] = $time.$u;
		
		$combine = array_combine($x, $upimage_title);
	
		if($_FILES['upimage']['name'][0]!='')
		{
			foreach($combine as $key => $val)
			{
				$dt = array('content_id' => $last_insert_id, 'title' => $val, 'image' => $key);
				$this->db->insert('assign_image_company', $dt); 
			}	
		}	
		
		if($_FILES['upimage']['name'][0]!='') { 
			foreach($_FILES['upimage']['tmp_name'] as $key => $tmp_name)
			{
				if($tmp_name) {
					$file_name = $_FILES['upimage']['name'][$key];
					$file_size =$_FILES['upimage']['size'][$key];
					$file_tmp =$_FILES['upimage']['tmp_name'][$key];
					$file_type=$_FILES['upimage']['type'][$key];  
					$fname = $time.$file_name;
					$dir = "./uploads/mcontent_company/";
					move_uploaded_file($tmp_name, $dir.$fname);				
				}
			}
		}
		
		if(!empty($_REQUEST['upvideo']))
		{
			foreach($_REQUEST['upvideo'] as $key => $val)
			{
				if($val) {
					$dt = array('content_id' => $last_insert_id, 'video_url' => $val);
					$this->db->insert('assign_video_url_company', $dt); 
				}
			}
		}
	}


function add_content_admin($data)
	{
		$this->db->insert('mcontent', $data); 
		$last_insert_id = $this->db->insert_id();
		$time = time();
		
		$upimage = $_FILES['upimage']['name'];
		$upimage_title = $_REQUEST['upimage_title'];		
		
		foreach($upimage as $u)
		$x[] = $time.$u;
		
		$combine = array_combine($x, $upimage_title);
	
		if($_FILES['upimage']['name'][0]!='')
		{
			foreach($combine as $key => $val)
			{
				$dt = array('content_id' => $last_insert_id, 'title' => $val, 'image' => $key);
				$this->db->insert('assign_image', $dt); 
			}	
		}	
		
		if($_FILES['upimage']['name'][0]!='') { 
			foreach($_FILES['upimage']['tmp_name'] as $key => $tmp_name)
			{
				if($tmp_name) {
					$file_name = $_FILES['upimage']['name'][$key];
					$file_size =$_FILES['upimage']['size'][$key];
					$file_tmp =$_FILES['upimage']['tmp_name'][$key];
					$file_type=$_FILES['upimage']['type'][$key];  
					$fname = $time.$file_name;
					$dir = "./uploads/mcontent/";
					move_uploaded_file($tmp_name, $dir.$fname);				
				}
			}
		}
		
		if(!empty($_REQUEST['upvideo']))
		{
			foreach($_REQUEST['upvideo'] as $key => $val)
			{
				if($val) {
					$dt = array('content_id' => $last_insert_id, 'video_url' => $val);
					$this->db->insert('assign_video_url', $dt); 
				}
			}
		}
	}
	
	function update_content($data, $mc_id)
	{	
		$this->db->where('mc_id', $mc_id);
		$this->db->update('mcontent', $data);
		$time = time();
		
		$upimage = $_FILES['upimage']['name'];
		$upimage_title = $_REQUEST['upimage_title'];
		
		
		foreach($upimage as $u)
		$x[] = $time.$u;
		
		$combine = array_combine($x, $upimage_title);

		
		if($_FILES['upimage']['name']!='') { 
			foreach($combine as $key => $val)
			{
				if($val) {
				$dt = array('content_id' => $mc_id, 'title' => $val, 'image' => $key);
				$this->db->insert('assign_image', $dt); 
				}
			}
		}	

		//add new image	
		foreach($_FILES['upimage']['tmp_name'] as $key => $tmp_name)
		{
			if($tmp_name) {
				$file_name = $_FILES['upimage']['name'][$key];
				$file_size =$_FILES['upimage']['size'][$key];
				$file_tmp =$_FILES['upimage']['tmp_name'][$key];
				$file_type=$_FILES['upimage']['type'][$key];  
				$fname = $time.$file_name;
				$dir = "./uploads/mcontent/";
				move_uploaded_file($tmp_name, $dir.$fname);				
			}
		}

		
		//remove old video url
		$this->db->delete('assign_video_url', array('content_id' => $mc_id)); 		
		
		//add new video url
		foreach($_REQUEST['upvideo'] as $key => $val)
		{
			if($val) {
				$dt = array('content_id' => $mc_id, 'video_url' => $val);
				$this->db->insert('assign_video_url', $dt); 
			}
		}		
						
		$imgtext = $this->input->post('imgtext');
		$imgtextid = $this->input->post('imgtextid');
		$comb = array_combine($imgtext, $imgtextid);
		foreach($comb as $key => $val) {
			$dt = array('title' => $key);
			$this->db->where('id', $val);
			$this->db->update('assign_image', $dt);
		}

	}



function update_content_comp($data, $mc_id)
	{	
		$this->db->where('mc_id', $mc_id);
		$this->db->update('mcontent_company', $data);
		$time = time();
		
		$upimage = $_FILES['upimage']['name'];
		$upimage_title = $_REQUEST['upimage_title'];
		
		
		foreach($upimage as $u)
		$x[] = $time.$u;
		
		$combine = array_combine($x, $upimage_title);

		
		if($_FILES['upimage']['name']!='') { 
			foreach($combine as $key => $val)
			{
				if($val) {
				$dt = array('content_id' => $mc_id, 'title' => $val, 'image' => $key);
				$this->db->insert('assign_image_company', $dt); 
				}
			}
		}	

		//add new image	
		foreach($_FILES['upimage']['tmp_name'] as $key => $tmp_name)
		{
			if($tmp_name) {
				$file_name = $_FILES['upimage']['name'][$key];
				$file_size =$_FILES['upimage']['size'][$key];
				$file_tmp =$_FILES['upimage']['tmp_name'][$key];
				$file_type=$_FILES['upimage']['type'][$key];  
				$fname = $time.$file_name;
				$dir = "./uploads/mcontent_company/";
				move_uploaded_file($tmp_name, $dir.$fname);				
			}
		}

		
		//remove old video url
		$this->db->delete('assign_video_url_company', array('content_id' => $mc_id)); 		
		
		//add new video url
		foreach($_REQUEST['upvideo'] as $key => $val)
		{
			if($val) {
				$dt = array('content_id' => $mc_id, 'video_url' => $val);
				$this->db->insert('assign_video_url_company', $dt); 
			}
		}		
						
		$imgtext = $this->input->post('imgtext');
		$imgtextid = $this->input->post('imgtextid');
		$comb = array_combine($imgtext, $imgtextid);
		foreach($comb as $key => $val) {
			$dt = array('title' => $key);
			$this->db->where('id', $val);
			$this->db->update('assign_image_company', $dt);
		}

	}
function update_content_admin($data, $mc_id)
	{	
		$this->db->where('mc_id', $mc_id);
		$this->db->update('mcontent', $data);
		$time = time();
		
		$upimage = $_FILES['upimage']['name'];
		$upimage_title = $_REQUEST['upimage_title'];
		
		
		foreach($upimage as $u)
		$x[] = $time.$u;
		
		$combine = array_combine($x, $upimage_title);

		
		if($_FILES['upimage']['name']!='') { 
			foreach($combine as $key => $val)
			{
				if($val) {
				$dt = array('content_id' => $mc_id, 'title' => $val, 'image' => $key);
				$this->db->insert('assign_image', $dt); 
				}
			}
		}	

		//add new image	
		foreach($_FILES['upimage']['tmp_name'] as $key => $tmp_name)
		{
			if($tmp_name) {
				$file_name = $_FILES['upimage']['name'][$key];
				$file_size =$_FILES['upimage']['size'][$key];
				$file_tmp =$_FILES['upimage']['tmp_name'][$key];
				$file_type=$_FILES['upimage']['type'][$key];  
				$fname = $time.$file_name;
				$dir = "./uploads/mcontent/";
				move_uploaded_file($tmp_name, $dir.$fname);				
			}
		}

		
		//remove old video url
		$this->db->delete('assign_video_url', array('content_id' => $mc_id)); 		
		
		//add new video url
		foreach($_REQUEST['upvideo'] as $key => $val)
		{
			if($val) {
				$dt = array('content_id' => $mc_id, 'video_url' => $val);
				$this->db->insert('assign_video_url', $dt); 
			}
		}		
						
		$imgtext = $this->input->post('imgtext');
		$imgtextid = $this->input->post('imgtextid');
		$comb = array_combine($imgtext, $imgtextid);
		foreach($comb as $key => $val) {
			$dt = array('title' => $key);
			$this->db->where('id', $val);
			$this->db->update('assign_image', $dt);
		}

	}
	
	function delete_content($id)
	{
		$this->db->delete('mcontent', array('mc_id' => $id)); 		
		$this->db->delete('assign_image', array('content_id' => $id)); 		
		$this->db->delete('assign_video_url', array('content_id' => $id)); 		
	}	
        function delete_content_comp($id)
	{
		$this->db->delete('mcontent_company', array('mc_id' => $id)); 		
		$this->db->delete('assign_image_company', array('content_id' => $id)); 		
		$this->db->delete('assign_video_url_company', array('content_id' => $id)); 		
	}	

function delete_content_admin($id)
	{
		$this->db->delete('mcontent', array('mc_id' => $id)); 		
		$this->db->delete('assign_image', array('content_id' => $id)); 		
		$this->db->delete('assign_video_url', array('content_id' => $id)); 		
	}

	function delete_image($id)
	{
		$this->db->delete('assign_image', array('id' => $id)); 		
	}

        function delete_image_comp($id)
	{
		$this->db->delete('assign_image_company', array('id' => $id)); 		
	}

function delete_image_admin($id)
	{
		$this->db->delete('assign_image', array('id' => $id)); 		
	}
	
	
	function update_status($id, $status)
	{
		$data = array(
		   'status' => $status
		);
		
		$this->db->where('mc_id', $id);
		$this->db->update('mcontent', $data); 
	}
        function update_status_comp($id, $status)
	{
		$data = array(
		   'status' => $status
		);
		
		$this->db->where('mc_id', $id);
		$this->db->update('mcontent_company', $data); 
	}


function update_status_admin($id, $status)
	{
		$data = array(
		   'status' => $status
		);
		
		$this->db->where('mc_id', $id);
		$this->db->update('mcontent', $data); 
	}

	
	function isTitleExist($title) {
		$this->db->select('mc_id');
		$this->db->where('title', $title);
		$query = $this->db->get('mcontent');

		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

       function isTitleExistComp($title) {
		$this->db->select('mc_id');
		$this->db->where('title', $title);
		$query = $this->db->get('mcontent_company');

		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
       function isTitleExistAdmin($title) {
		$this->db->select('mc_id');
		$this->db->where('title', $title);
		$query = $this->db->get('mcontent');

		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}
	


	function isTitleExistInUpdate($title, $id) {
		$this->db->select('mc_id');
		$this->db->where('title', $title);
		$this->db->where('mc_id != '.$id.'');
		$query = $this->db->get('mcontent');

		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}



function isTitleExistInUpdate_comp($title, $id) {
		$this->db->select('mc_id');
		$this->db->where('title', $title);
		$this->db->where('mc_id != '.$id.'');
		$query = $this->db->get('mcontent_company');

		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

function isTitleExistInUpdate_admin($title, $id) {
		$this->db->select('mc_id');
		$this->db->where('title', $title);
		$this->db->where('mc_id != '.$id.'');
		$query = $this->db->get('mcontent');

		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	
	function get_group()
    {
		$this->db->select('*');
		$this->db->from('group');
        $query = $this->db->get();
        return $query->result();
    }
    function get_usersComp()
    {
		$this->db->select('group_concat(user_id) as user_id');
		$this->db->from('user');
		$this->db->where('user_type != "admin"');
		$this->db->where('status', 1);
        	$query = $this->db->get();
		$name='';
		foreach( $query->result() as $q) {
			$name = $q->user_id;
		}
	        return $name;
    }
    function get_comp_admin()
    {
		$this->db->select('group_concat(user_id) as user_id');
		$this->db->from('user');
                $this->db->where('user_type', 'user');
		$this->db->where('status', 1);
        	$query = $this->db->get();
		$name='';
		foreach( $query->result() as $q) {
			$name = $q->user_id;
		}
	        return $name;
    }

    
    function get_users()
    {
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('user_type != "admin"');
		$this->db->where('status', 1);
        $query = $this->db->get();
        return $query->result();
    }

    
    function username_byid($uids){ 
		$sql = "SELECT name FROM user WHERE user_id IN (".$uids.")"; 
		$query = $this->db->query($sql);
		$name = '';
        foreach( $query->result() as $q) {
			$name .= $q->name.'<br/>';
		}
		return $name;
	}
	
	function groupname_byid($gids){ 
		$sql = "SELECT group_name FROM `group` WHERE group_id IN (".$gids.")"; 
		$query = $this->db->query($sql);
		$gname = '';
        foreach( $query->result() as $q) {
			$gname .= $q->group_name.'<br/>';
		}
		return $gname;
	}
	
	function get_image($mc_id){ 
		$this->db->select('id, image, title');
		$this->db->from('assign_image');
		$this->db->where('content_id', $mc_id);
		$this->db->order_by('image', 'asc');
        $query = $this->db->get();
        return $query->result();
	}

     function get_image_comp($mc_id){ 
		$this->db->select('id, image, title');
		$this->db->from('assign_image_company');
		$this->db->where('content_id', $mc_id);
		$this->db->order_by('image', 'asc');
        $query = $this->db->get();
        return $query->result();
	}

function get_image_admin($mc_id){ 
		$this->db->select('id, image, title');
		$this->db->from('assign_image');
		$this->db->where('content_id', $mc_id);
		$this->db->order_by('image', 'asc');
        $query = $this->db->get();
        return $query->result();
	}
	
	function get_title($mc_id){ 
		$this->db->select('content_id, title');
		$this->db->from('assign_title');
		$this->db->where('content_id', $mc_id);
		$this->db->order_by('title', 'desc');
        $query = $this->db->get();
        return $query->result();
	}
	
	function get_video_url($mc_id){ 
		$this->db->select('video_url');
		$this->db->from('assign_video_url');
		$this->db->where('content_id', $mc_id);
        $query = $this->db->get();
        return $query->result();
	}
function get_video_url_comp($mc_id){ 
		$this->db->select('video_url');
		$this->db->from('assign_video_url_company');
		$this->db->where('content_id', $mc_id);
        $query = $this->db->get();
        return $query->result();
	}


function get_video_url_admin($mc_id){ 
		$this->db->select('video_url');
		$this->db->from('assign_video_url');
		$this->db->where('content_id', $mc_id);
        $query = $this->db->get();
        return $query->result();
	}
	
	function get_preview_image($id){ 
		$this->db->select('*');
		$this->db->from('assign_image');
		$this->db->where('id', $id);
		$this->db->order_by('image', 'asc');
        $query = $this->db->get();
        return $query->result();
	}


       function get_preview_image_comp($id){ 
		$this->db->select('*');
		$this->db->from('assign_image_company');
		$this->db->where('id', $id);
		$this->db->order_by('image', 'asc');
        $query = $this->db->get();
        return $query->result();
	}

function get_preview_image_admin($id){ 
		$this->db->select('*');
		$this->db->from('assign_image');
		$this->db->where('id', $id);
		$this->db->order_by('image', 'asc');
        $query = $this->db->get();
        return $query->result();
	}

	function chkall_inactive($ids, $status)
	{
		$sql  = 'UPDATE `mcontent` SET `status` = '.$status.' WHERE `mc_id` IN ('.$ids.')';
		$update = $this->db->query($sql);
	}

       function chkall_inactive_comp($ids, $status)
	{
		$sql  = 'UPDATE `mcontent_company` SET `status` = '.$status.' WHERE `mc_id` IN ('.$ids.')';
		$update = $this->db->query($sql);
	}
function chkall_inactive_admin($ids, $status)
	{
		$sql  = 'UPDATE `mcontent` SET `status` = '.$status.' WHERE `mc_id` IN ('.$ids.')';
		$update = $this->db->query($sql);
	}

	function chkall_active($ids, $status)
	{
		$sql  = 'UPDATE `mcontent` SET `status` = '.$status.' WHERE `mc_id` IN ('.$ids.')';
		$update = $this->db->query($sql);
	}

        function chkall_active_comp($ids, $status)
	{
		$sql  = 'UPDATE `mcontent_company` SET `status` = '.$status.' WHERE `mc_id` IN ('.$ids.')';
		$update = $this->db->query($sql);
	}
function chkall_active_admin($ids, $status)
	{
		$sql  = 'UPDATE `mcontent` SET `status` = '.$status.' WHERE `mc_id` IN ('.$ids.')';
		$update = $this->db->query($sql);
	}
	function chkall_delete($ids, $status)
	{
		$sql  = 'DELETE FROM `mcontent` WHERE `mc_id` IN ('.$ids.')';
		$update = $this->db->query($sql);
	}
        function chkall_delete_comp($ids, $status)
	{
		$sql  = 'DELETE FROM `mcontent_company` WHERE `mc_id` IN ('.$ids.')';
		$update = $this->db->query($sql);
	}
function chkall_delete_admin($ids, $status)
	{
		$sql  = 'DELETE FROM `mcontent` WHERE `mc_id` IN ('.$ids.')';
		$update = $this->db->query($sql);
	}
}
?>
