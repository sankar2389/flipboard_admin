<?php
Class Webservice_model extends CI_Model
{
	public function __construct() {
		parent::__construct();
	}

	function check_login($data) {
		$this->db->select('*');
		$this->db->where('username', $data['username']);
		$this->db->where('password', $data['password']);
		$query = $this->db->get('user');

		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return 0;
		}
	}	
	
	function check_login_byid($id = 0) {
		$this->db->select('*');
		$this->db->where('user_id', $id);
		$query = $this->db->get('user');

		if ($query->num_rows() > 0) {
			return 1;
		} else {
			return 0;
		}
	}
	
	function change_pass($current_pass, $new_pass, $userid)
	{
		$value['result'] = 'Change password';
		$this->db->select('*');
		$this->db->where('user_id',$userid); 
		$dbpassword = $this->db->get('user')->row()->password;
		if($dbpassword==$current_pass)
		{
			$data = array(
			   'password' => $new_pass
			);			
			$this->db->where('user_id', $userid);
			$this->db->update('user', $data);
			$value['message'] = 'Password updated';		
			$value['status'] = 1;		
			echo json_encode($value);	
		} else {
			$value['message'] = 'Current password is incorrect';		
			$value['status'] = 0;		
			echo json_encode($value);	
		}
	}
	
	function get_content()
	{
		$this->db->select('*');
		$this->db->from('mcontent');
		$this->db->where('status', 1);
		$this->db->order_by('mc_id', 'desc');
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			foreach($query->result() as $r)
			{
				$mc_id = $r->mc_id;
				$title = $r->title;
				$author = $r->author;
				$content = $r->description;
				$post_url = $r->post_url;
				$timestamp = $r->created_date;
				$date = date('d-M-Y', strtotime($timestamp));
				$time = date('h:i a', strtotime($timestamp));
				$images = $this->get_images($mc_id);
				$video_url = $this->get_video_url($mc_id);
		
				$data[] = array('title' => $title, 
						'content' => $content,
						'post_url' => $post_url,
						'author' => $author,
						'date' => $date,
						'time' => $time,
						'image_url' => $images,
						'video_url' => $video_url
					);
				
			}				
			echo json_encode($data);	
		} else {
			$data['message'] = 'No content found';		
			$data['status'] = 0;		
			echo json_encode($data);	
		}
	}
	
	function get_content_byid($id=0)
	{
		
		$this->db->select('*');
		$this->db->from('mcontent');
		$this->db->like('share_user_id', $id);
		$this->db->where('status', 1);
		$this->db->order_by('mc_id', 'desc');
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			foreach($query->result() as $r)
			{
				$mc_id = $r->mc_id;
				$author = $r->author;
				$title = $r->title;
				$content = $r->description;
				$post_url = $r->post_url;
				$timestamp = $r->created_date;
				$date = date('d-M-Y', strtotime($timestamp));
				$time = date('h:i a', strtotime($timestamp));
				$images = $this->get_images($mc_id);
				$video_url = $this->get_video_url($mc_id);				

				$data[] = array('title' => $title, 
					'content' => $content,
					'post_url' => $post_url,
					'author' => $author,
					'date' => $date,
					'time' => $time,
					'video_url' => $video_url,
					'image_url' => $images
				);
				
			}	
			echo json_encode($data);	
		} else {
			$data['message'] = 'No content found';		
			$data['status'] = 0;		
			echo json_encode($data);	
		}
	}
		//get Content By bookID
function get_content_bookbyid($id=0)
{
	// echo "<pre>yyy";
	// echo $id;
	
	$this->db->select('*');
	$this->db->from('mcontent');
	//$this->db->like('share_user_id', $id);
	$this->db->like('book_id', $id);
	$this->db->where('status', 1);
	$this->db->order_by('mc_id', 'desc');
	$query = $this->db->get();
	
	if ($query->num_rows() > 0) {
		foreach($query->result() as $r)
		{
			$mc_id = $r->mc_id;
			$author = $r->author;
			$title = $r->title;
			$content = $r->description;
			$post_url = $r->post_url;
			$timestamp = $r->created_date;
			$date = date('d-M-Y', strtotime($timestamp));
			$time = date('h:i a', strtotime($timestamp));
			$images = $this->get_images($mc_id);
			$video_url = $this->get_video_url($mc_id);				

			$data[] = array('title' => $title, 
				'content' => $content,
				'post_url' => $post_url,
				'author' => $author,
				'date' => $date,
				'time' => $time,
				'video_url' => $video_url,
				'image_url' => $images
			);
			
		}	
		echo json_encode($data);	
	} else {
		$data['message'] = 'No content found';		
		$data['status'] = 0;		
		echo json_encode($data);	
	}
}

	function get_content_byid_old($id=0)
	{
		$data['result'] = 'Get Content By ID';
		$this->db->select('*');
		$this->db->from('mcontent');
		$this->db->like('share_user_id', $id);
		$this->db->where('status', 1);
		$this->db->order_by('mc_id', 'desc');
		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			foreach($query->result() as $r)
			{
				$mc_id = $r->mc_id;
				$title = $r->title;
				$content = $r->description;
				$post_url = $r->post_url;
				$timestamp = $r->created_date;
				$date = date('d-M-Y', strtotime($timestamp));
				$time = date('h:i a', strtotime($timestamp));
				$images = $this->get_images($mc_id);
				$video_url = $this->get_video_url($mc_id);
				
				$data['data'][] = array('title' => $title, 
					'content' => $content,
					'post_url' => $post_url,
					'date' => $date,
					'time' => $time,
					'image_url' => $images,
					'video_url' => $video_url
				);
			}	
			$data['status'] = 1;
			$out[] = $data;	
			echo json_encode($out);	
		} else {
			$data['message'] = 'No content found';		
			$data['status'] = 0;		
			echo json_encode($data);	
		}
	}
	
	function get_images($content_id) {
		$this->db->select('title, image');
		$this->db->from('assign_image');
		$this->db->where('content_id', $content_id);
		$query = $this->db->get();
		$img = array();
		
		if ($query->num_rows() > 0) {
			
			foreach($query->result() as $i) {	
				if($i->image!='') {							
				$whitelist = array('127.0.0.1', '::1');
				if(!in_array($_SERVER['REMOTE_ADDR'], $whitelist))
					$image_url = 'http://www.'.$_SERVER['HTTP_HOST'].'/sankar2389/admin/uploads/mcontent/'.$i->image; // live image_url
				else
					$image_url = site_url('uploads/mcontent/'.$i->image); // local image_url
					
				$img[] = array('title' => $i->title, 'image' => $image_url);
				} else
					return array();
				
			}
			
			return $img;
			
		} else {
			return array();
		}
	}	
	
	function get_video_url($content_id) {
		$this->db->select('video_url');
		$this->db->from('assign_video_url');
		$this->db->where('content_id', $content_id);
		$query = $this->db->get();
		$vid = array();
		
		if ($query->num_rows() > 0) {			
			foreach($query->result() as $v) {						
				$vid = $v->video_url;
			}			
			return $vid;			
		} else {
			return 0;
		}
	}	
	
}
?>