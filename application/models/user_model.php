<?php
Class User_model extends CI_Model
{
	public function __construct() {
		parent::__construct();
	}
	
	public function record_count() {
		$txt_search = $this->input->post('txt_search');
		if($txt_search!='') {
			$st="(name LIKE '%".$txt_search."%' OR email LIKE '%".$txt_search."%' OR username LIKE '%".$txt_search."%')";
  			$this->db->where($st);  
		}
		$this->db->where('username != "admin"');
		$this->db->order_by("user_id", "desc");		
		$query = $this->db->get("user");
		return $query->num_rows();
	}
	
	public function fetch_users($limit, $start) {
		$this->db->limit($limit, $start);	
		$txt_search = $this->input->post('txt_search');
		if($txt_search!='') {		
			$st="(name LIKE '%".$txt_search."%' OR email LIKE '%".$txt_search."%' OR username LIKE '%".$txt_search."%')";
  			$this->db->where($st);  
		}
		$this->db->where('username != "admin"');
		$this->db->order_by("user_id", "desc");	
		$query = $this->db->get("user");
		//echo $this->db->last_query();
	
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		} else {
			$data = 0;
			return $data;
		}		
	}
	   
	function login($username, $password)
	{
		$this->db->select('user_id, username, password, user_type, status');
		$this->db->from('user');
		$this->db->where('username', $username);
		$this->db->where('password', $password); 
		$this->db->limit(1);
		
		$query = $this->db->get();
		
		if($query->num_rows() == 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

function loginCompany($username, $password)
{

   

  		$this->db->select('group_id, group_name, password,status,first_time_login');
		$this->db->from('group');
		$this->db->where('group_name', $username);
		$this->db->where('password', $password); 
                $this->db->limit(1);
                $query = $this->db->get();
		
		if($query->num_rows() == 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}


}	
	  
    function get_group()
    {
		$this->db->select('*');
		$this->db->from('group');
        $query = $this->db->get();
        return $query->result();
    }
	
	function get_features_byid($userid)
    {
		$this->db->select('*');
		$this->db->from('menu_ref');
		$this->db->join('menu', 'menu_ref.menu_id = menu.menu_id', 'left');
		$this->db->where('menu_ref.user_id', $userid);
		
		$query = $this->db->get();
        return $query->result();
    }
	
	function add_user()
	{	
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$dept = $this->input->post('department');
		$designation = $this->input->post('designation');
		$username = $name;
		$password = $this->random_password();
		$data = array(
		   'name' =>  $name,
		   'email' =>  $email,
		   'department' =>  $dept,
		   'designation' =>  $designation,
		   'username' => $username ,
		   'password' => $password ,
		   'status' => 1,
		   'user_type' => 'user',
		   'first_time_login' => 'no'
		);		
				
		$this->db->insert('user', $data); 
		$user_id = $this->db->insert_id();	
		$group = $this->input->post('group');
		if(!empty($group))
		{
			$imp = implode($group, ','); 
			$value = array('group' => $imp);			
			$this->db->where('user_id', $user_id);
			$this->db->update('user', $value); 
		}
		
		// Email to user
		$this->emailtousers($name, $username, $password, $email);	

				
		$features = $this->input->post('features');
		if(!empty($features))
		{
			foreach($features as $val)
			{
				//$menu_name = $this->get_menuname_byid($val);
				$data = array(
				   'menu_id' => $val ,
				   'user_id' => $user_id
				);
				$this->db->insert('aquad_menu_ref', $data); 
			}
		}
		
		return true;				
	}

	function emailtousers($name=NULL, $username=NULL, $password=NULL, $email=NULL)
	{	
		$to = $email;
		$from = 'admin@test.com';
		$fromname = 'Sankar Flipboard';
		$sub = "Thank you for your Registration.";
		$message='<table width="100%" bgcolor="#dfdfdf" border="0" cellspacing="10" cellpadding="0" style="padding-top:10px;">
		  <tr>
			<td><table width="700" border="0" align="center" cellpadding="0" cellspacing="0" style="background:#ffffff; margin-top:0px;">
			
				<tr>
				  <td bgcolor="#2d2d2d" height="36" style="font-size:12px;	font-family:Arial, Helvetica, sans-serif;	color:#e4e4e4;	text-align:left;	font-weight:bold; padding-left:14px;">Registration</td>
				</tr>
				<tr>
				  <td bgcolor="#30B1F2" style="line-height:3px;">&nbsp;</td>
				</tr>
				<tr style="background:#00A9F5;">
				  <td style="line-height:11px;">LOGO</td>
				</tr>
				<tr>
				  <td style="font-size:12px;	font-family:Arial, Helvetica, sans-serif;	color:#000009;	text-align:left; padding:15px 0 5px 15px;	font-weight:bold;">Dear <span style="font-size:12px;	font-family:Arial, Helvetica, sans-serif;	color:#024186;	text-align:left; font-weight:bold;">'.$name.',</span></td>
				</tr>
				
				<tr>
				  <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#3f3f3f; background:#fafafa; line-height:30px; text-align:center;"><div class="footer-con">
					  <p>It is a Amazing Day and Thank you for registering with this APP</p>
					</div></td>
				</tr>
				<tr>
				  <td style="padding-top:0px; padding-bottom:10px;"><table width="75%" border="0" align="center" cellpadding="0" cellspacing="0" style="border:1px solid #e8e9eb;  -webkit-border-radius: 3px; -moz-border-radius: 3px;border-radius: 3px; margin-top:18px">
					  <tr>
						<td style="padding:3px;"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" >
							<tr>
							  <td><table width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#fbfbfb;">
								  <tr>
									<td width="36%" height="33" style="font-size:12px;	font-family:Arial, Helvetica, sans-serif;	color:#6d6d6d;	text-align:left;	padding-left:15px;"> Your Username:</td>
									<td width="1%" style="font-size:12px;	font-family:Arial, Helvetica, sans-serif;	color:#6d6d6d;	text-align:left;">:</td>
									<td width="63%"  style="font-size:12px;	font-family:Arial, Helvetica, sans-serif;	color:#6d6d6d;	text-align:left;">'.$username.'</td>
								  </tr>
								  <tr>
									<td height="33" style="font-size:12px;	font-family:Arial, Helvetica, sans-serif;	color:#6d6d6d;	text-align:left;	padding-left:15px;">Your Password:</td>
									<td style="font-size:12px;	font-family:Arial, Helvetica, sans-serif;	color:#6d6d6d;	text-align:left;">:</td>
									<td  style="font-size:12px;	font-family:Arial, Helvetica, sans-serif;	color:#6d6d6d;	text-align:left;">'.$password.'</td>
								  </tr>
									<tr>
							   </table></td>
							</tr>
						  </table></td>
					  </tr>
					</table></td>
				</tr>
				<tr>
				  <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#3f3f3f; background:#fafafa; line-height:30px; text-align:center;"><div class="footer-con">
					  <p><a href="http://superearth.in/sankar2389/admin" style="color:purple;">Click to Login</a></p>
					</div></td>
				</tr>
				<tr>
								  <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#3f3f3f; background:#fafafa; line-height:30px; text-align:center;"><div class="footer-con">
									  <p>Please login with your account and signup for your subscription<br /> 
									  Thank you, <strong style="color:#0fb2e4;">Team - Sankar Flipboard</strong></p>
									</div></td>
								</tr>
				<tr>
				  <td bgcolor="#30B1F2" style="line-height:1px;">&nbsp;</td>
				</tr>
		
		
			  </table></td>
		  </tr>
		</table>';		
				
		$headers  = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type: text/html; charset=utf-8" . "\r\n";
		$headers .= "From: $fromname <$from>" . "\r\n";
		mail($to, $sub, $message, $headers);
	}

	function randomuser( $length = 8 ) {
    	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    	$password = substr( str_shuffle( $chars ), 0, $length );
    	return $password;
	}

	function random_password( $length = 8 ) {
    	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    	$password = substr( str_shuffle( $chars ), 0, $length );
    	return $password;
	}
	
	function update_user($id)
	{						
		$data = array(
		   'name' => $this->input->post('name') ,
		   'email' => $this->input->post('email') ,
		   'department' => $this->input->post('department') ,
		   'designation' => $this->input->post('designation') ,
		   'status' => 1,
		   'user_type' => 'user'
		);
		
		//clear old group value
		$value = array('group' => '');			
		$this->db->where('user_id', $id);
		$this->db->update('user', $value); 
		
		//update new selected group value
		$group = $this->input->post('group');
		if(!empty($group))
		{
			$imp = implode($group, ','); 
			$value = array('group' => $imp);			
			$this->db->where('user_id', $id);
			$this->db->update('user', $value); 
		}
		
		$this->db->where('user_id', $id);
		$this->db->update('user', $data); 		

		return true;				
	}
	
	function update_status($id, $status)
	{
		$data = array(
		   'status' => $status
		);
		
		$this->db->where('user_id', $id);
		$this->db->update('user', $data); 
	}
	
	function delete_user($id)
	{
		$this->db->delete('user', array('user_id' => $id)); 		
	}	

	function isEmailExist($email) {
		$this->db->select('user_id');
		$this->db->where('email', $email);
		$query = $this->db->get('user');

		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	function isEmailExistInUpdate($email, $id) {
		$this->db->select('user_id');
		$this->db->where('email', $email);
		$this->db->where('user_id != '.$id.'');
		$query = $this->db->get('user');

		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	function get_user($id = 0) 
	{
		$this->db->select('*');
        $this->db->where('user_id', $id);
        $query = $this->db->get('user');		
        return $query->result();
	}
	
	function get_group_byid($gid){ 
		$this->db->select('group_name');
		$this->db->from('group');
		$this->db->where('group_id',$gid); 
		return $this->db->get()->row()->group_name;
	}
	
	function change_pass($current_pass, $new_pass, $userid)
	{
		$this->db->select('password');
		$this->db->from('user');
		$this->db->where('user_id',$userid); 
		$dbpassword = $this->db->get()->row()->password;
		if($dbpassword==$current_pass)
		{
			$data = array(
			   'password' => $new_pass
			);			
			$this->db->where('user_id', $userid);
			$this->db->update('user', $data);
			$this->session->set_flashdata('success_msg', 'Password updated');
			redirect('change_pass'); 
		} else {
			$this->session->set_flashdata('error_msg', 'Current password is incorrect');
			redirect('change_pass');
		}
	}

       function change_pass_company($current_pass, $new_pass, $userid)
	{
		$this->db->select('password');
		$this->db->from('group');
		$this->db->where('group_id',$userid); 
		$dbpassword = $this->db->get()->row()->password;
		if($dbpassword==$current_pass)
		{
			$data = array(
			   'password' => $new_pass
			);			
			$this->db->where('group_id', $userid);
			$this->db->update('group', $data);
			$this->session->set_flashdata('success_msg', 'Password updated');
			redirect('change_pass_company'); 
		} else {
			$this->session->set_flashdata('error_msg', 'Current password is incorrect');
			redirect('change_pass_company');
		}
	}

	
	function edit_profile($data, $userid)
	{
		$this->db->where('user_id', $userid);
		$this->db->update('user', $data);
	}
	
	function chkall_inactive($ids, $status)
	{
		$sql  = 'UPDATE `user` SET `status` = '.$status.' WHERE `user_id` IN ('.$ids.')';
		$update = $this->db->query($sql);
	}

	function chkall_active($ids, $status)
	{
		$sql  = 'UPDATE `user` SET `status` = '.$status.' WHERE `user_id` IN ('.$ids.')';
		$update = $this->db->query($sql);
	}

	function chkall_delete($ids, $status)
	{
		$sql  = 'DELETE FROM `user` WHERE `user_id` IN ('.$ids.')';
		$update = $this->db->query($sql);
	}

}
?>
