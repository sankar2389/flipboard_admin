-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 19, 2019 at 04:02 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `superear_traco`
--

-- --------------------------------------------------------

--
-- Table structure for table `assign_image`
--

CREATE TABLE `assign_image` (
  `id` int(111) NOT NULL,
  `content_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assign_image`
--

INSERT INTO `assign_image` (`id`, `content_id`, `title`, `image`) VALUES
(1, 3, 'test user image', '1566219520DSC_4384.jpg'),
(2, 5, 'Beautiful Image', '1566222212DSC_0850.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `assign_image_admin`
--

CREATE TABLE `assign_image_admin` (
  `id` int(111) NOT NULL,
  `content_id` int(111) NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `assign_video_url`
--

CREATE TABLE `assign_video_url` (
  `id` int(111) NOT NULL,
  `content_id` int(11) NOT NULL,
  `video_url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assign_video_url`
--

INSERT INTO `assign_video_url` (`id`, `content_id`, `video_url`) VALUES
(1, 4, 'https://www.youtube.com/watch?v=XL_3eYx3OVE'),
(2, 6, 'https://www.youtube.com/watch?v=U0V1y2p1sgs');

-- --------------------------------------------------------

--
-- Table structure for table `assign_video_url_admin`
--

CREATE TABLE `assign_video_url_admin` (
  `id` int(111) NOT NULL,
  `content_id` int(111) NOT NULL,
  `video_url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `book_master`
--

CREATE TABLE `book_master` (
  `bk_id` int(111) NOT NULL,
  `book_title` text NOT NULL,
  `user_id` varchar(20) NOT NULL,
  `img_path` text NOT NULL,
  `active` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_master`
--

INSERT INTO `book_master` (`bk_id`, `book_title`, `user_id`, `img_path`, `active`, `created_date`) VALUES
(1, 'Flowers History', '1', '1566221429DSC_0033.jpg', 1, '2019-08-19 13:36:26');

-- --------------------------------------------------------

--
-- Table structure for table `group`
--

CREATE TABLE `group` (
  `group_id` int(111) NOT NULL,
  `group_name` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `users` text DEFAULT NULL,
  `status` int(11) NOT NULL,
  `first_time_login` varchar(10) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group`
--

INSERT INTO `group` (`group_id`, `group_name`, `password`, `users`, `status`, `first_time_login`) VALUES
(1, 'velsure', 'pKNjLsRV', '2', 1, 'No');

-- --------------------------------------------------------

--
-- Table structure for table `mcontent`
--

CREATE TABLE `mcontent` (
  `mc_id` int(111) NOT NULL,
  `title` text NOT NULL,
  `login_user_id` int(11) NOT NULL,
  `type` text NOT NULL,
  `description` text NOT NULL,
  `post_url` text NOT NULL,
  `author` text NOT NULL,
  `book_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `share_group_id` int(11) NOT NULL,
  `share_user_id` varchar(20) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mcontent`
--

INSERT INTO `mcontent` (`mc_id`, `title`, `login_user_id`, `type`, `description`, `post_url`, `author`, `book_id`, `status`, `share_group_id`, `share_user_id`, `created_date`) VALUES
(3, 'test user image', 2, 'Image', 'test user image', 'www.google.com', 'suresh', 0, 1, 1, '2', '2019-08-19 12:58:40'),
(4, 'test user video', 2, 'Video', 'test user video', 'www.google.com', 'suresh', 0, 1, 1, '2', '2019-08-19 13:00:20'),
(5, 'flower  images', 1, 'Image', 'flower images', 'http://google.com ', 'admin', 1, 1, 2, '2', '2019-08-19 13:43:32'),
(6, 'admin video', 1, 'Video', 'admin video', 'http://google.com ', 'admin', 1, 1, 2, '2', '2019-08-19 13:44:27');

-- --------------------------------------------------------

--
-- Table structure for table `mcontent_admin`
--

CREATE TABLE `mcontent_admin` (
  `mc_id` int(111) NOT NULL,
  `type` varchar(20) NOT NULL,
  `description` text NOT NULL,
  `post_url` text NOT NULL,
  `author` text NOT NULL,
  `status` int(11) NOT NULL,
  `share_group_id` int(11) NOT NULL,
  `share_user_id` varchar(11) NOT NULL,
  `title` text NOT NULL,
  `login_user_id` int(111) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mcontent_admin`
--

INSERT INTO `mcontent_admin` (`mc_id`, `type`, `description`, `post_url`, `author`, `status`, `share_group_id`, `share_user_id`, `title`, `login_user_id`, `created_date`) VALUES
(1, 'Image', 'Funny images', 'www.google.com', 'admin', 1, 1, '', 'Funny images', 1, '2019-08-19 11:35:05'),
(2, 'Video', 'fun ny videos description ', 'www.google.com', 'admin', 1, 1, '', 'funny videos', 1, '2019-08-19 11:41:46');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `department` text NOT NULL,
  `designation` text NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_type` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `first_time_login` varchar(10) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `name`, `email`, `department`, `designation`, `username`, `password`, `user_type`, `status`, `first_time_login`) VALUES
(1, 'admin', 'admin@gmail.com', '', '', 'admin', '123456', 'admin', 1, 'no'),
(2, 'suresh', 'suresh@gmail.com', '', '', 'suresh', '123456', 'user', 1, 'yes');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assign_image`
--
ALTER TABLE `assign_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assign_image_admin`
--
ALTER TABLE `assign_image_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assign_video_url`
--
ALTER TABLE `assign_video_url`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_master`
--
ALTER TABLE `book_master`
  ADD PRIMARY KEY (`bk_id`);

--
-- Indexes for table `group`
--
ALTER TABLE `group`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `mcontent`
--
ALTER TABLE `mcontent`
  ADD PRIMARY KEY (`mc_id`);

--
-- Indexes for table `mcontent_admin`
--
ALTER TABLE `mcontent_admin`
  ADD PRIMARY KEY (`mc_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assign_image`
--
ALTER TABLE `assign_image`
  MODIFY `id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `assign_image_admin`
--
ALTER TABLE `assign_image_admin`
  MODIFY `id` int(111) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `assign_video_url`
--
ALTER TABLE `assign_video_url`
  MODIFY `id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `book_master`
--
ALTER TABLE `book_master`
  MODIFY `bk_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `group`
--
ALTER TABLE `group`
  MODIFY `group_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mcontent`
--
ALTER TABLE `mcontent`
  MODIFY `mc_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `mcontent_admin`
--
ALTER TABLE `mcontent_admin`
  MODIFY `mc_id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
